## This tutorial shows how to run LZ simulation, convert .bin to .root and plot
photon distribution.

### Prerequisits
* NERSC credentials
* authorized to login to Cori/Edison
* 'repo' with non-zero allocation
* pre-compiled LZ code at location: ~/LUXSim-Feb2017/
* this  tutorial git-repo placed at ~/tutorNersc/2017_lz 

### Step 1 : login to Edison, launch interactive session on Edison, use shared queue, request 4 vCore machine

[laptop]
ssh -A -X edison.nersc.gov

[balewski@edison login node]

salloc --ntasks 4 --partition shared --time 20:00 

[balewski@nid02302  interactive session]
cat /etc/*release    # verify OS=SUSE:12 is NOT what you want
lstopo -c | grep PU | nl    # verify you got 4 vCores


### step 2 : activate OS consistent w/ LZ pre-compiled code, setup LZ environment
module load shifter
shifter --image=custom:pdsf-chos-sl64:v1 bash

[bash-4.1 inside your OS]
cat /etc/*release  # will show OS you want: Scientific Linux release 6.4 


source ~/tutorNersc/2017_lz/myLzSetup.source
cd ~/LUXSim-Feb2017


### step 3 : define parameters of a particular LZ simulation. Here we want 500 Quick-events written to directory out2 on $CSCRATCH

mkdir $CSCRATCH/out2 
export OUTPUT_DIR=$CSCRATCH/out2
export   RND_SEED=1122
export    NUM_EVE=500

time ./LUXSimExecutable ~/tutorNersc/2017_lz/LZQuickCheckNoVis.mac >& $OUTPUT_DIR/log-quick-1&

# wait 40 seconds, then check your job is still running:

top ibn1
#  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                                                                     
# 63853 balewski  20   0 1504m 1.4g  21m R 98.8  1.1   0:43.16 LUXSimExecutabl                                                             


# job finishes with:
# real	1m15.577s
# user	0m55.788s
# sys	0m6.980s

### step 4 : convert .bin to .root and plot some observables
 ./tools/LUXRootReader $OUTPUT_DIR/LZQuickCheckNoVis_$RND_SEED.bin

root -l ~/tutorNersc/2017_lz//qaOneLzPlot.C'("LZQuickCheckNoVis_$RND_SEED","$OUTPUT_DIR")'

#### cleanup and exit 3x
bash-4.1$ exit # from shifter


balewski@nid02086:~> exit  # from  interactive node
#  salloc: Relinquishing job allocation 3831915

balewski@cori07:~> exit  # from cori
#  Connection to cori.nersc.gov closed.


### step 3+4b: produce & plot one 5keV event
mkdir $CSCRATCH/out2 
export OUTPUT_DIR=$CSCRATCH/out2
export   RND_SEED=1133
export    NUM_EVE=1
ls -l ~/tutorNersc/2017_lz/singleER_5keV_serial.mac 
time ./LUXSimExecutable ~/tutorNersc/2017_lz/singleER_5keV_serial.mac >& $OUTPUT_DIR/log-5kev-1&

 ./tools/LUXRootReader $OUTPUT_DIR/singleER_5keV_serial_$RND_SEED.bin

root -l ~/tutorNersc/2017_lz//qaOneLzPlot.C'("singleER_5keV_serial_$RND_SEED","$OUTPUT_DIR")'


