qaOneLzPlot(TString core="a", TString inpPath=".", int mode=1){

  //inpPath="/global/project/projectdirs/mpccc/balewski/lz-pdsf-mod1-8/";
  //core="FullSlowSimulation_ER_flat_10000";

  inpPath+="/";
  TString fname=inpPath+core+".root";

  TChain *chain = new TChain("tree");
  fd=new TFile(fname); assert(fd->IsOpen());

  chain->Add(fname);
  int  nHit=chain->GetEntries();
  printf("open %s , hits=%d mode=%d\n", fname.Data(),nHit,mode);

  assert(nHit>0);

  header->Draw("numPrimaries>>h2");
  // header->Print();
  //chain->Show(0);
  
  float nEve=h2->GetMean();
  TH1F* h1 = new TH1F("h1",Form("%s, prim tracks=%.0f; num Optical Photons/ prim track",core.Data(),nEve), 250, 0., 5000.);

  TCanvas *can = new TCanvas("canvas", core, 800, 600);
  can->SetBottomMargin(0.12);


  chain->Draw("iTotOptNum>>h1","");
  gPad->SetLogy(1);

  can->cd(2);
  //float nHit=h1->GetEntries();
  float xAvr=h1->GetMean();
  float xRms=h1->GetRMS();
  //printf("eee=%s\n",auxTxt.Data());
  printf("{\"coreR\":\"%s\",\"numPrim\":%.0f,\"numOptPhot\":%.0f,\"eneMean\":%.3f,\"eneRms\":%.3f,\"timeSTR\":timeTBD,\"node\":nodeTBD}\n",core.Data(),nEve,nHit,xAvr,xRms);
  //
  gPad->SetLogy();

  if(mode==1)  can->Print(inpPath+core+".pdf");
}

 

