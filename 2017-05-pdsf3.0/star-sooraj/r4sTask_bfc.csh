#!/bin/tcsh 
# Note, all code in this scrpt is executed under tcsh
    echo  task-in-shifter
    echo inShifter:`env|grep  SHIFTER_RUNTIME`
    cat /etc/*release
    #
    # - - - -  D O   N O T  T O U C H  T H I S   S E C T I O N- - - - 
    #
    whoami    
    echo  load STAR enviroment in shifter
    set NCHOS = sl64
    set SCHOS = 64
    set DECHO = 1
    set SCRATCH = $WRK_DIR/out-star1
    setenv GROUP_DIR /common/star/star${SCHOS}/group/
    source $GROUP_DIR/star_cshrc.csh    
    # Georg fix for tcsh
    setenv LD_LIBRARY_PATH /usr/common/usg/software/gcc/4.8.2/lib:/usr/common/usg/software/java/jdk1.7.0_60/lib:/usr/common/usg/software/gcc/4.8.2/lib64:/usr/common/usg/software/mpc/1.0.3/lib/:/usr/common/usg/software/gmp/6.0.0/lib/:/usr/common/usg/software/mpfr/3.1.3/lib/:$LD_LIBRARY_PATH
    echo
    echo avaliable STAR-lib version in this OS image:
    #ls -d /common/star/star64/packages/SL*
    #
    # - - - -   Y O U   C A N   C H A N G E   B E L O W  - - - -
    #    


    set daqN = $argv[1]
     
    echo  starting new-r4s  daqN=$daqN, OUT_DIR=$OUT_DIR, workerName=`hostname -f`, startDate=`date`


    if ( !  -f $daqN ) then
	echo "ERROR: file ${daqN} does not exist, Aborting-33"
	exit
    endif
    echo size of daqN
    ls -l  $daqN 
     
    echo testing STAR setup $STAR_VER in `pwd`
    starver $STAR_VER 
    env |grep STAR

    echo 'my new STAR ver='$STAR'  test root4star '
    root4star -b -q 
    if ( $? != 0) then
	echo STAR environment is corrupted, aborting job
	echo $STAR
	which root4star
	exit
    endif
 
    #echo EEEEE ;   exit

    echo `date`" Fire: r4s for daqN=$daqN   [wiat]"

    /usr/bin/time -v  root4star runqaPicoDst.sh $daqN >& $LOG_PATH/r4s-${SLURM_JOB_ID}.log
    echo `date`" completed job $daqN  , save results to "$OUT_DIR
    ls -l
    time cp *LcPairTree.root $OUT_DIR
    echo 'copy done '`date`

