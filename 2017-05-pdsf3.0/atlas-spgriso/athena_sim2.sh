#!/bin/bash

  echo  task-in-shifter
  echo inShifter:`env|grep  SHIFTER_RUNTIME`
  cat /etc/*release
  export CHOS=sl64
  #source /usr/share/Modules/init/bash
  #module use /usr/common/usg/Modules/modulefiles
  #source ~/.bash_profile.ext

#Settings
ASetupStr="AtlasProduction,20.20.8.4,here"

#List of dynamically needed expansions (in the code with __XXX__)
# BASE_DIR -> Test area
# OUTDIR -> Base output directory
# SUBDIR -> optional sub-directory


# this is local scratch on Cori, flushed after 12 weeks
coreN=atlas_sim2
TMPDIR=$CSCRATCH/${coreN}-${SLURM_JOBID}-${SLURM_ARRAY_TASK_ID}
mkdir -p $TMPDIR

BASEDIR="/global/homes/s/spgriso/code/AtlasItkAnalogClus/runStep17ChargeCalib/sim/"

echo "run on node "`hostname`

SUBDIR="revTilt.v1"
if ! [ -z $1 ]; then
 OUTDIR=$1
fi

if ! [ -z $2 ]; then
 SUBDIR=$2
fi

echo list directories as used
echo TMPDIR=$TMPDIR
echo BASEDIR=$BASEDIR
echo OUTDIR=$OUTDIR
echo SUBDIR=$SUBDIR

myMaxEvents=${EVE_PER_TASK}
mySkipEvents=0

# use job array to split tasks 

if ! [ -z "${SLURM_ARRAY_TASK_ID}" ] ; then
    mySkipEvents=$(((${SLURM_ARRAY_TASK_ID}-1)*${EVE_SKIP_STEP}))
fi

echo "=============================="
echo "sample = $SAMPLE,  SLURM_ARRAY_TASK_ID=${SLURM_ARRAY_TASK_ID}="
echo "myMaxEvents = ${myMaxEvents},  mySkipEvents = ${mySkipEvents}"
echo "=============================="

echo "Starting batch job. Processing sample: $SAMPLE"

#Setup ATLAS software on PDSF
shopt -s expand_aliases
#source /common/atlas/scripts/setupATLAS.sh
source /global/homes/s/spgriso/janTest/mySetup/setupATLAS.sh
setupATLAS
lsetup asetup

echo "Setting up ATLAS software $ASetupStr in $BASEDIR/"
cd $BASEDIR/
asetup ${ASetupStr}


echo "------ ENVIRONMENT ---------"
printenv
echo "---- END OF ENVIRONMENT ----"
echo ""

CPYDIR=$OUTDIR/$SUBDIR
echo "Output: ${CPYDIR}"
mkdir -pv ${CPYDIR}

cd $TMPDIR
WRKDIR=$PWD
echo "Work dir: ${PWD}"

echo deploy delayed monitor
(sleep 240;  echo Jan dump information after delay; date; top ibn1 ; free -g; ulimit -a)&

# Setup job settings and run it
time  Sim_tf.py \
--geometryVersion ATLAS-P2-ITK-10-00-03_VALIDATION \
--conditionsTag OFLCOND-MC15c-SDR-14-01 \
--preInclude InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.SiliconOnly.py,InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py \
--postInclude InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py \
--postExec 'ServiceMgr.DetDescrCnvSvc.DoInitNeighbours = False' \
--DataRunNumber=241000 \
--inputEVNTFile $SAMPLE --outputHITSFile HITS.${SGE_TASK_ID}.pool.root \
--maxEvents ${myMaxEvents} --skipEvents=${mySkipEvents}

# >& log.SimTf

#compress log file
gzip $WRKDIR/log.*

#List output
echo ""
echo "List of local output files:"
echo "ls -lh ${WRKDIR}/"
ls -lh $WRKDIR/

#now copy the output files and log file to output location
echo ""
echo "Now copying to ${CPYDIR}"
mv $WRKDIR/*root* $CPYDIR/
for pippo in $WRKDIR/log.*; do
  newName=`echo $pippo | sed 's/gz$/'${SGE_TASK_ID}'.gz/g'`
  mv $pippo $newName
done
mv $WRKDIR/log.* $CPYDIR/
echo ""
echo "Content of destination: $CPYDIR"
ls -lh $CPYDIR/
