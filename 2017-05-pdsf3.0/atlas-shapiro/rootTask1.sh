#!/bin/bash
if [[ $SLURM_JOB_PARTITION == *"-chos" ]]
then
   echo  task-in-chos
   chosenv
   ls -l /proc/chos/link
else
  echo  task-in-shifter
  #source /usr/share/Modules/init/bash
  #module use /usr/common/usg/Modules/modulefiles
  echo inShifter:`env|grep  SHIFTER_RUNTIME`
  cat /etc/*release
  export CHOS=sl64
  #source ~/.bash_profile.ext
fi

inputfile=${1-fixMe1}
echo task1-inp: inputfile=$inputfile WRK_DIR=$WRK_DIR  OUT_DIR=$OUT_DIR
date

if [ ! -f $inputfile ] ; then
     echo "ERROR: file ${inputfile} does not exist!i, Aborting-33"
     exit
fi
echo inputfile
ls -lh $inputfile
cd $WRK_DIR

echo "I AM HERE:",$PWD
shopt -s expand_aliases
source /common/atlas/scripts/setupATLAS.sh
setupATLAS
echo Prepare a local copy of MG can edit it
time( cp /global/projecta/projectdirs/atlas/haichen/HGAF/packages.tar.gz . ; tar zxvf packages.tar.gz)

cd packages
rcSetup Base 2.4.27
rcSetup
mkdir tmpp
cd tmpp;
ln -s $inputfile .
name=`ls`;
cd -;
ln -s $inputfile test_mxAOD.root

mkdir  output_for_MVA;
date
(sleep 120;  echo Dump1 information after 2min delay; date; top ibn1 ; free -g)&
(sleep 600;  echo Dump2 information after 10min delay; date; top ibn1 ; free -g)&
echo  ' start-task in '`pwd`
/usr/bin/time -v  runLBLttHAnalysis LBLttH2yy/data/test.cfg
echo 'task-done at '`date`

df -h output_for_MVA/test/
ls -tlr output_for_MVA/test/
echo storing output 
time cp -rp output_for_MVA/test/* $OUT_DIR
echo root-done
