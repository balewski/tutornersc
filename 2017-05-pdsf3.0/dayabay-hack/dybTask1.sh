#!/bin/bash
if [[ $SLURM_JOB_PARTITION == *"-chos" ]]
then
   echo  task-in-chos
   chosenv
   ls -l /proc/chos/link
else
  echo  task-in-shifter  
  echo inShifter:`env|grep  SHIFTER_RUNTIME`
  cat /etc/*release
  export CHOS=sl53
  unset LS_COLORS
fi

echo use WRK_DIR=$WRK_DIR=
source ~/.bashrc.ext
source ~hack/cosmic

source ~/batch
export EDIMAGES=$WRK_DIR
export EDSHARE=/global/homes/h/hack/Parameters
export EDOFFSETS=$WRK_DIR

time nuwa.py -n 7777 -Anone --dbconf=offline_db  -m 'MuDi.MuDi -t .000.Test_ -Q 300 -o 50 -q 50 -r 69648 -U WR -T M -C -p qt -f 0 -F 1 -v TSl -E 1 -G A -J' /global/homes/h/hack/Parameters/runLists/eh1/eh1_69648/eh1_69648_000.list > $WRK_DIR/test.log 2> $WRK_DIR/test.elg

echo 'task1-done at '`date`
ls -tlr .

 