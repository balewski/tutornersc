# Tips for efficient use of SLURM on PDSF

PDSF SLURM offers only the 'shared' partition, meaning: a single SLURM job can access between 1-60 vCores, 
multiple SLURM jobs will run concurently on the same node. The coexistence of up to 60 identical task on the same node
 can cause inefficiencies if they compete for a sparse resource , like acces to the local disc. 
The tips below will help you to avoid some of the traps and your jobs will run efficiently:


1. [Use local scratch](#loc-scratch)
2. [Avoid IO contention](#IO-contention)
	1. spread job start
	2. copy tar balls
	3. detect  task stuck in the **D-state**
	4. check free space before you start batch of jobs
3. [Varia](#varia)
	1. print host name
	2. bash is preferred
4. more

### 1. Use local scratch
  <a name="loc-scratch"></a>
DO:

* use $SLURM_TMP  for local scratch located on the node running your task. It is auto-erased after your job completes, even if it crashes.


AVOID:

* do not use /tmp , which has size of only 0.5GB and is never cleaned up.



### 2. Avoid IO contention 
<a name="IO-contention"></a>
Keep in mind that the local disc has just few heads and there may be 60 concurrent tasks requesting IO.
If this happen you will see your task(s) will be demoted to the 'D' state, for many minutes.
 Such D-state task will block the CPU and do nothing. To avoid it:

2.1 **Spread job start**

If you submit many identical jobs, e.g. using the job-array pattern:

```bash
$ sbatch sbatch --array=1-500 myJob.slr
```
it may happen all of them start at once (within 0.1 second).
 Typically, PDSF resources are not agile enough to serve 500 concurent, identical requests. 
Consider to add a random delay before the resource consuming  operation. Always time your operations with 'time':

``` bash
sleep $(($RANDOM % 30))  # add random delay up to 120 seconds 
time cp -rp ~/jan/zillion_files_stash  $SLURM_TMP  # copy gazilion of (small) files
```

Distinguish 'cp ~/bigCalibFile $SLURM_TMP' at the task start from  the case of your analysis code writing to $SLURM_TMP.
 The later happens in small chunks, after some computation is done (some time passes)  and it is less likely 60 such task will do it precisely in the same time.

2.2 **Copy tar balls**

If you need to copy a fragmented idirectory tree, e.g. Madgraph source code or many small output files do rather local tar/untar and copy the tar-ball between  discs.

``` bash
# manually, one time:
cd /project/.../myMadgraph
tar -cf madSource.tar madSource/

#inside each SLURM task
cp /project/.../myMadgraph/madSource.tar $SLURM_TMP
cd $SLURM_TMP
time tar -xf madSource.tar
make
simu3.exe
```

2.3 Detect  task stuck in the **D-state**

A simple trick is to insert the  delayed execution of the 'top' command and watch for the output in the slurm log file.
 You need to guess which of potentially many steps of your analysis executed in sequence by the single SLURM job may be problematic. 
Choose the time delay to coincide with the expected timing of the suspecious operation.

E.g., let assume simu2.exe will read a large local file 3 minutes after it is started. This content of the myTask.sh task description executed by myJob.slr SLURM job would do the trick:
```bash
$ cat myTask.sh
#!/bin/bash
...
(sleep 180; top ibn1 )&
time simu2.exe
...
```

Identify the block show below in the SLURM logfile. If you see many of your tasks in 'D' state, like here this innocent 'cp' command - you need to change your workflow.

```bash
PID USER PR NI VIRT RES SHR S %CPU %MEM TIME+ COMMAND 
14436 balewski 20 0 14528 2804 824 R 7.4 0.0 0:00.06 top 
1748 kkrizka 20 0 111m 1048 752 D 1.8 0.0 0:08.63 cp 
6266 kkrizka 20 0 111m 1052 752 D 1.8 0.0 0:08.71 cp 
1749 kkrizka 20 0 111m 1048 748 D 0.0 0.0 0:08.82 cp 
1750 kkrizka 20 0 111m 1044 748 D 0.0 0.0 0:08.72 cp 
1751 kkrizka 20 0 111m 1052 752 D 0.0 0.0 0:08.79 cp 
4441 kkrizka 20 0 111m 1052 752 D 0.0 0.0 0:08.85 cp 
4443 kkrizka 20 0 111m 1048 752 D 0.0 0.0 0:08.81 cp 
```

You may want to take few snapshots with multiple 'top' executed in the background after various delays.

2.4 Check free space before submitting jobs

Check your home space

```bash
pdsf6 $ myquota
Displaying quota usage for user balewski:
                  ---------- Space (GB) ---------  --------------- Inode ---------------
FileSystem           Usage      Quota     InDoubt      Usage        Quota        InDoubt
----------------  ---------  ---------  ---------  -----------  -----------  -----------
HOME                     24         40          0       424651      1000000           40
```

Your /project space, e.g. for STAR do

```bash
pdsf6 $ prjquota star
             ---------- Space (GB) ---------     ------------- Inode --------------
Project          Usage      Quota    InDoubt          Usage       Quota     InDoubt
----------   ---------  ---------  ---------     ----------  ----------  ----------
star             65989      71680          1       19519639    20000000         453
```

### 4. Varia

<a name="varia"></a>
Various tricks improving debugging of your jobs.

4.1  Print host name inside the shell script running your task. Use grep 'start-A' to catch it up for log file. It will help you if things go wrong. Add this line on the top of your myTask.sh script.

```bash
#!/bin/bash
echo "start-A host "`hostname`"  date "`date`
```

4.2  use bash shell for SLURM script  - bash is better debugged at NERSC. It is fine to use tcsh for the task script.



