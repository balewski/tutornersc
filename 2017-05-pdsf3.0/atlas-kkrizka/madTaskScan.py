#!/usr/bin/env python
''' needs those modules

Cori:
 module load root
 module load python/3.5-anaconda

PDSF sl64:
 module load python/3.4.3

'''

jobID0='27568'

import sys
import os
#import subprocess
#import json
import numpy as np
from pprint import pprint

print ('check python ver=',sys.version)
(va,vb,vc,vd,ve)=sys.version_info
#print (va,vb)
if va<3:
    print ('proceed... ver2')
else:
    print ('proceed... ver3')
assert(va==3)

# units tests ---------

logPath='/global/homes/k/kkrizka/janTest3/fire/'
print (logPath)

badPhrasesL=[]
badPhrasesL.append('slurm_script: fork: Resource temporarily unavailable')


################################
def get_sec(st):
    x1 = st.split('m')
    #print (st, len(x1 ))
    t=0.
    if len(x1)==2:
        t+=float(x1[0])*60.
        x2=x1[1].split('s')
        if len(x2)==2:
            t+=float(x2[0])
    return t


#------------------------
def getSlurmLogs(path,myID):
    outL=os.listdir(path)
    outD={}
    for x in outL:
        if  myID not in x: continue
        if  '.swp'  in x: continue
        #print(x)
        xx=x[6:-4]
        xL=xx.split('_')
        #print(xL)
        arr=xL[1]
        outD[xx]={'log':path+x,'arrId':int(arr)}
        #print(outD[xx])
    return outD
        

#------------------------
def readOneLogFile( inpF ):  
   outD={}
   #print ('readOneLogFile inp:',inpF)
   statInfo = os.stat(inpF) # in Bytes
   #print ('statInfo=',statInfo)  
   sizekB= float(statInfo.st_size)/1024.
   #print (sizekB)
   outD['logSize_kB']=sizekB

   state=0 # 0= unknow, 1=begin of record, 2=inside record
   numRec=0

   badC={} # for counting lines w/ given phrase
   for badX in badPhrasesL:
       badC[badX]=0

   with open(inpF) as fd:
       nodeName='none'
       copyIn_sec=0.
       eveGen_sec=0.
       totWall_sec=0.
       for line in fd:
            if len(line)<=1:  continue
            
            for badX in badPhrasesL:
                if badX in line : badC[badX]=badC[badX]+1

            if  state<1 and 'start-A' in line:
                #print(line)
                xL=line.split()
                nodeName=xL[1]
                state=2
                continue

            if  state>1 and 'real' in line:
                if 'priority' in line: continue
                #print(line)
                xL=line.split()
                #print(xL)
                copyIn_sec=get_sec(xL[1])
                #print(copyIn_sec)
                state=5
                continue

            if  state==5 and 'INFO: Refine results' in line:
                state=10

            if  state==10 and 'current time' in line:
                line1=line
                state=11


            if  state==11 and 'INFO:  Idle: 0,  Running: 0' in line:
               line2=line
               #print(line1)
               #print(line2)
               xL=line2.split()
               #print(xL)
               tmStr=xL[8]+xL[9]
               eveGen_sec=get_sec(tmStr)
               #print(eveGen_sec)
               state=20

            if  state==20 and 'Elapsed (wall clock) time' in line:
               xL=line.split()
               #print(xL)
               tmStr=xL[7].replace(':', 'm')+'s'
               totWall_sec=get_sec(tmStr)
               #print(totWall_sec)
               state=30

             
       outD['nodeName']=nodeName
       outD['copyIn_sec']=copyIn_sec
       outD['eveGen_sec']=eveGen_sec
       outD['totWall_sec']=totWall_sec

       # compact badC
       badFlags=[]
       for i, badX in enumerate(badPhrasesL):
           if badC[badX] : badFlags.append(i)
       outD['badFlags']=badFlags

   return outD

#------------------------
def doAnalysisByNode(jobD):
    outD={}
    wrkD={}
    for core in jobD:
        rec=jobD[core]
        #print('all',rec)
        #print(core,rec['nodeName'], rec['logSize_kB'])
        nodeN=rec['nodeName']
        if nodeN not in outD: 
            outD[nodeN]={'nJob':0, 'anyBadFlag':0}
            wrkD[nodeN]={ 'logSizeL':[], 'totWallL':[], 'copyInL':[]}
        # accumulate list for AVR calc.
        outD[nodeN]['nJob']=outD[nodeN]['nJob']+1
        wrkD[nodeN]['logSizeL'].append(rec['logSize_kB'])

        xx=rec['copyIn_sec']
        if xx>0:
            wrkD[nodeN]['copyInL'].append(xx)

        xx=rec['totWall_sec']
        if xx>0:
            wrkD[nodeN]['totWallL'].append(xx)

        if len(rec['badFlags'])>0 : 
             outD[nodeN]['anyBadFlag']=outD[nodeN]['anyBadFlag']+1

    for nodeN in outD:
        #if 'mc1528' in nodeN: print(wrkD[nodeN]['totWallL'])
        sizeV=np.array(wrkD[nodeN]['logSizeL'])
        wallV=np.array(wrkD[nodeN]['totWallL'])
        copyV=np.array(wrkD[nodeN]['copyInL'])
        outD[nodeN]['logSize_kB']={'mean':np.mean(sizeV),'std':np.std(sizeV)}
        outD[nodeN]['totWall_sec']={'mean':np.mean(wallV),'std':np.std(wallV),'cnt':wallV.size}
        outD[nodeN]['copyIn_sec']={'mean':np.mean(copyV),'std':np.std(copyV),'cnt':copyV.size}

        outD[nodeN]['smallLog']=0 # init to no errors yet
        outD[nodeN]['totWall']={'short':0, 'long':0} 
                           
    return outD

#------------------------
def jobScan2(jobD,statD):
    nLogS=0; nLogL=0
    nWallS=0; nWallL=0
    for core in jobD:
        rec=jobD[core]
    
        nodeN=rec['nodeName']
        if nodeN =='none': continue

        stat=statD[nodeN]['logSize_kB']
        wall=statD[nodeN]['totWall_sec']

        #sizeThrL=stat['mean']-1* stat['std']
        sizeThrL=stat['mean']/2. # (kB)
        
        #print('sizeThrL',nodeN,sizeThrL)
        sizeThrH=stat['mean']+1* stat['std']

        wallTS=wall['mean']-4* stat['std']
        wallTL=wall['mean']+4* stat['std']

        if rec['totWall_sec'] < wallTS :
            nWallS=nWallS+1
            statD[nodeN]['totWall']['short']=statD[nodeN]['totWall']['short']+1 
        if rec['totWall_sec'] > wallTL :
            nWallL=nWallL+1
            statD[nodeN]['totWall']['log']=statD[nodeN]['totWall']['long']+1 

        if rec['logSize_kB'] < sizeThrL :
            nLogS=nLogS+1
            statD[nodeN]['smallLog']= statD[nodeN]['smallLog']+1
            #print('small:',rec['nodeName'],rec['log'],statD[nodeN]['smallLog'])
        
        if rec['logSize_kB'] > sizeThrH :
            nLogL=nLogL+1
            #print('large:',rec['nodeName'],rec['log'])
        

    print('doScan2 end: logs S=',nLogS,' L=',nLogL,' wallT S=',nWallS,' L=',nWallL)

#------------------------

#====================
#  M A I N 
#===================

print("-------")
print("-------")
print("-------")

jobD=getSlurmLogs(logPath,jobID0)

print(" MAIN , query tasks from list=%s  size =%d"%(logPath,len(jobD)))

nr=0
for core in jobD:
    nr+=1
    #core='26558_20' # bad
    #core='26558_181' # good
    rec=jobD[core]
    #print(nr,core,rec)
    outD=readOneLogFile( rec['log'] )
    rec.update(outD)
    #print('new',rec); break
#bbb

print('done w/ scan ...')
statD=doAnalysisByNode(jobD)
#pprint(statD); aaa


print('fish for cases...')
jobScan2(jobD,statD)

#pprint(statD)

totJob=0
totGood=0
for nodeN in sorted(statD.keys()):
    D=statD[nodeN]
    wallD=D['totWall_sec']

    goodJob=D['nJob'] - D['totWall']['short'] -  D['totWall']['long']
    anyJob=D['nJob'] 
    if 'none' in nodeN : goodJob=0 
    totJob=totJob+anyJob
    totGood=totGood+goodJob
    frac=goodJob/anyJob

    txt1=' avrWallT(%d)= %.1f+/-%.1f(sec) '%(wallD['cnt'],wallD['mean'],wallD['std'])
    txt2=' copyInT(%d)= %.1f+/-%.1f(sec) '%(D['copyIn_sec']['cnt'],D['copyIn_sec']['mean'],D['copyIn_sec']['std'])
    print(nodeN,' allJobs=',D['nJob'], ' smallLog=',D['smallLog'],' anyBadFlag=',D['anyBadFlag'],txt1
          ,txt2          ,goodJob, '%.2f'%frac)

print('Summary JOB=',jobID0,' nTotJob=',totJob,'  goodJob=',totGood,'  fractGood=%.2f'%(totGood/totJob))
