###  Bulk transfer of data  from NERSC to S3 bucket  (Worksheet 6 ver1.1) 

#### Prerequisites
 * Account at jgi-workflows project @ Google
 * web-facing directory at NERSC
 * large data files to be transfered from NERSC
 * in this exericse you will NOT use your personal/google-sdk Docker instance
 
 Note, you can use a web-facing directory on any other facility.


Let the variable $discPath points to the web-exported disc at NERSC where you can write. Let $urlCore is the URL for this disc location. Note, your directory must be opened for read to be visible via web.
```bash
[my choice]
ssh cori.nersc.gov
urlCore=http://portal.nersc.gov/project/star/balewski/gce-test/
discPath=/project/projectdirs/star/www/balewski/gce-test
$ ls -ld .
drwxr-sr-x  /project/projectdirs/star/www/balewski/gce-test/

$ cd $discPath 
$ echo 'test1' >data0.txt
$ chmod 0644 data0.txt
```
 Q: do you see content of data0.txt in the browser pointed at $urlCore?

 * If you do not have large files handy you can copy few 1GB files  from this location. 
```bash
[any NERSC machine]
$ cd $discPath
$ wget http://portal.nersc.gov/project/star/balewski/gce-test-keep/big1000MB.1.dat && chmod 0446 big1000MB.1.dat
$ cd -
```
The following files exist  big1000MB.[1-4].dat (check it w/ the browser)

* If you want to generate more or different size files you can customize the macro 'create_bigData.sh' and execute it.

#### 1) Prepare TCV manifest
To transfer files to Google S3 bucket with GCS transfer service you need to prepare a plain text list containing file names, size  and the md5 sums encode in base-64.
Below is an example of TCV file:
```bash
TsvHttpData-1.0
https://example.com/buckets/obj1      1357      RbZHOOIY2a1CnPuc8ZOY0Q==
https://example.com/buckets/obj2      2468      xSe8XGGbDbSChLw6VU7
```
 * The macro 'prepGCS_TSV_dataList.sh' will prepare such list for you - execute it in any directory. **Make sure the variables point to yours  $discPath and $urlCore are correct.** The produced TCV list file is named 'dataList.gcs.tsv' and will be placed also  $discPath.
```bash
[any NERSC machine]
$ git clone https://bitbucket.org/balewski/tutorNersc
$ cd tutorNersc/2017-03-google-gce/
$ vi prepGCS_TSV_dataList.sh
$ ./prepGCS_TSV_dataList.sh
 
   work on 0 big1000MB.1.dat
   work on 1 big1000MB.4.dat
   #totals: nFile=2 sumGB=2
   your url= http://portal.nersc.gov/project/star/balewski/gce-test/dataList.gcs.tsv
```
 * verify you can download to your laptop the  dataList.gcs.tsv in the browser pointed to $urlCore

#### 2) Transver data using GCS WEB GUI

 * Select 'Product & Services' (main menue)
 * Select 'Storage'
 * Select 'Transfer'
 * Select 'Create Transfer Job'
 * Item 1 'Source'. Select  'List of object URLs' and drop  URL for your TCV file, press 'Continue'
 ```bash
 http://portal.nersc.gov/project/star/balewski/gce-test/dataList.gcs.tsv
 ```
  * Item 2 'Destination' Point browser to your S3 bucket. My bucket is named: 'jan-tanker22', press 'Select'. Next, press 'Continue'
 * Item 3 'Configure transfer' Accept the default, just press 'Create'
 
To monitor progress press the 'Transfer' button on the left, select your 'transfer task' and observe if your transfer has completed. Typical data transfer speed is of 100MB/sec. 2GB should take < 1 minute.

 * After transfer completes press 'browse' button  in the 'Storage' section. Find your bucket (my is jan-tanker-22) and follow the path to your files to see if they are there
 Note, the URL of your original location will be translated in to a directory tree: 
 ```bash
 Buckets/jan-tanker22/portal.nersc.gov/project/star/balewski/gce-test
 /big1000MB.1.dat , .../big1000MB.4.dat
 ```
 
#### 3) Cleanup 
 
 If you do not need files in S3 delete them  - or you will acquire unnecessary cost.
 

