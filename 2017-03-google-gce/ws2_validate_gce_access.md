## Validate GCE credentials on your laptop   (Worksheet-2)
(Proceed to the next step only if the outcome of the current step is good.)

###  Prerequisites
 * Account at jgi-workflows project @ Google - you have done the following before
```bash
On your Laptop, in any browser :  login to Google w/ your LBL user name 
Select 'Product & services' tab (top left), select 'Compute Engine' icon, you should see the 'Create' button
```
  *  Deploy on your laptop Docker image **balewski/ubu14-google-sdk** containing Google  command-line tools. You have already execute steps below on your laptop in an X-terminal.

```bash
[your laptop]
$ docker -v
    Docker version 17.03.0-ce    # update if your version is older
$ docker pull  balewski/ubu14-google-sdk
$ docker images | grep ubu14
    balewski/ubu14-google-sdk   latest    8da7133f85e4    935 MB
$ docker run -it balewski/ubu14-google-sdk
[root@sdk-instance]  
$$  ping 8.8.8.8 -c 3  # check Ethernet speed
   64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=3.11 ms
# and/or print (instantly) a lot of 'junk' with command
$$ curl http://www.google.com 
$$ exit   #quit from sdk-instance 
[laptop] 
```

### 1) Establish the trust between your google-sdk instance and the project jgi-workflows @ Google for the user being yourself 

This is the most complex part of this instruction. 
You need to do it only once. In the worse case scenario fire 
up a new sdk Docker instance and start 
over - this will give you a clean slate.


  * Launch again ( but differently) google-sdk image on your laptop. 
 Initially, you will have admin 
  privileges, verify the OS is Ubuntu14, list existing users (should be only one: balewski)
  
 
```bash
[laptop]
$ docker run -it --name trustMe balewski/ubu14-google-sdk
[root@trustMe]
$$ cat /etc/*release
       DISTRIB_DESCRIPTION="Ubuntu 14.04.5 LTS"
$$ ls /home
    balewski 
```
  Note, if you see the error 'The container name "/trustMe" is already in use' execute 'docker rm trustMe' and try again.

 * Your NERSC user name is not in /home, **add yourself**. Replace 'smith' with  your NERSC user name. You do not need to setup password for this new user, since you always enter the Docker image as root. Assume to be yourself.
```bash
[root@trustMe]
$$ adduser smith --gecos ""  --disabled-password
$$ su - smith
[smith@trustMe]
$ whoami
   smith
$ gcloud -h   # verify 'gcloud' tool kit is working for you.  
   Usage: gcloud [optional flags] <group | command>
  . . . 
$ ping 8.8.8.8 -c 3   #  check connection (again) 
      64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=3.11 ms
```

 * You are now yourself inside google-sdk instance running on your laptop. 
 Next, you will establish the trust with jgi-workflow project @ Google. Later, you will **save the modified local Docker 
 image so it is permanent**. You do not want to repeat them after your laptop reboots. 
 The commands below were executed for user=balewski. Only the lines requiring your answers are shown. 
 Select the zone to be 'us-west1-a'  - for this zone you may see a different index than 17. 
 
```bash
[root@trustMe]
$$ su - balewski
[balewski@trustMe]
$ gcloud init
   Welcome! This command will take you through the configuration of gcloud.
   ...
   You must log in to continue. Would you like to log in (Y/n)?  y
   Go to the following link in your browser:
      https://accounts.google.com/o/oauth2/auth?......very-long-urel....
   Enter verification code: [paste here your auth code from your browser]
   You are logged in as: [balewski@lbl.gov].

   Pick cloud project to use: 
    [1] jgi-workflows    <== YOU WANT THIS PROJECT
    [2] pdsf-workflows
   Please enter numeric choice  (must exactly match list item):  1

   Do you want to configure Google Compute Engine 
    (https://cloud.google.com/compute) settings (Y/n)? y 

   Which Google Compute Engine zone would you like to use as project default?
    [1] asia-east1-b
    ...
    [13] us-central1-c
    [14] us-east1-c
    [15] us-east1-d
    [16] us-east1-b
    [17] us-west1-a   <== YOU WANT THIS ZONE
    [18] us-west1-b
    [19] Do not set default zone
   Please enter numeric choice (must exactly match list item):  17  
   . . .
  Your Google Cloud SDK is configured and ready to use!
```
FYI, created  output  configuration w/ your credentials will be saved in the file named [/home/balewski/.boto] inside google-sdk instance.


 * Verify you can talk to jgi-workflow @ Google. The command 'gcloud ... list'  will list all running VMs 
 (if any) or display nothing, but it should NOT throw an error. Both outcomes are shown below. 
 If you see the 'BAD answer' sth went wrong - fix it before proceeding.
 
```bash
[balewski@trustMe]
$ gcloud compute instances list  #  GOOD answer ( or no output is good too)
     NAME       ZONE  MACHINE_TYPE   PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP     STATUS
     instance-jan32  us-west1-a  n1-standard-1 10.138.0.2   104.199.123.48 

$ gcloud compute instances list  #  BAD  answer:
     ERROR: (gcloud.compute.instances.list) The required property is not currently set.
     You may set it for your current workspace by running: ......
```
### 2) Save  this modified google-sdk as 'personal/google-sdk'

instance   w/ your embedded  jgi-workflow credentials.
Your running google-sdk instance has name 'trustMe'.  
Open a **different** X-terminal on your laptop and find this running instance and save it as a new Docker image. 

 *There are many ways to accomplish it. 
If you know Docker and like more some alternative path - go ahead using your way.*  

```bash
[laptop-x2]$ docker ps
CONTAINER ID    IMAGE               COMMAND   CREATED                   NAMES
82edc7880faf   balewski/ubu14-google-sdk   "/bin/bash"  19 minutes ago   trustMe
```
 * Do you see 'trustMe' at the end of the line? If no sth went wrong -fix it. 
 If yes - save this instance as new Docker image. Note, you can pick any name of your new Docker image file, 
 I have chosen 'smith/google-sdk'. Type this 'docker commit' command on your other X-term running on your laptop:

```bash
 [laptop-x2]
 $ docker commit -m ok trustMe personal/google-sdk           
    sha256:03d19f9401dccee98f67cf729719f6386bfb18abf0
 $ docker images |grep sdk
       personal/google-sdk              latest     03d19f9401dc        2 minutes ago   935 MB
       balewski/ubu14-google-sdk     latest     8da7133f85e4        25 hours ago    935 MB
```
* That is it - if you see 2 saved Docker images.  Now you can shut down the 'trustMe' Docker 
instance  - just type 'exit' in the 1st x-terminal.

### 3) Finally, verify the new 'personal/google-sdk' Docker image  contains your jgi-workflows credentials. 
Launch it in Docker and list running VMs @jgi-workflows project - there should be no errors.
In a new x-terminal on your laptop execute, replace 'balewski' with your LBL user name.

```bash
[laptop]
$ docker run -it personal/google-sdk
[root@personal] 
$$ su - balewski
[balewski@personal]
$  gcloud compute instances list 
   Listed 0 items.  (or perhaps sth is running and is listed, but no ERROR)
$ exit
$$ exit
[laptop] # you are back, all is closed
```
## If you got to this place you have completed  the setup. Congratulations!
Your new Docker image is ready to use and will allow you to remotely manipulate VMs on Google Cloud.

# WARNING: Do NOT publish your modified google-sdk image in Docker Hub. 
It contains your JGI credentials to Google Cloud. 
Anyone can pull public images from Docker Hub and use it to run computation at Google Cloud to be charged to JGI credit card.
