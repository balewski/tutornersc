## Compute on multi-core VM @ Google using private Docker image (Worksheet-3) 
(proceed to the next step only if the outome of current step is good)


### Prerequisites
 * Account at jgi-workflows project @ Google
 * On your laptop: **personal/google-sdk** image with your  credentials against jgi-workflows project.
 
### 1) Launch 4-vCore VM w/ 900 GB local disc at Google cloud. 
 * on your laptop start personal/google-sd
 * use 'gcloud' to start VM @ google. It may take few minutes, wait for completion
 * replace 'yourName' with a uniqe name. Here string 'jan' is used.
 * check how many VMs are you running (before and after VM launch)
```bash
[laptop] $ docker run -it personal/google-sdk
[root@personal] 
$$ su - balewski
[balewski@personal]
$  gcloud compute instances list   # check running VMs
    Listed 0 items.
    
$ gcloud compute instances create yourName-4v  --image-project coreos-cloud --image-family coreos-stable --machine-type=n1-standard-4 --boot-disk-size=900
    (takes 10-30 seconds)
    NAME ZONE MASTER_VERSION  MASTER_IP  TYPE VERSION   NUM_NODES    STATUS
    jan-4v  us-west1-a 1.5.3  35.185.194.219  n1-standard-4  1.5.3  1 RUNNING
    
$  gcloud compute instances list   # do you see your VM?
    NAME   ZONE        MACHINE_TYPE   .... EXTERNAL_IP
    jan-4v   us-west1-a  n1-standard-4  ...    35.185.196.94 
```
Note, this standardized  4-vCore VM costs $0.19/hour, incremented every minute. 
### 2) Connect to VM using web interface
 * Select 'Product & Services' (main menu)
 * Select 'Compute Engine'
 * Click on 'SSH' button (far right) for the VM you have launched.
 * Check disk size,  OS , RAM+swap, count vCores, and Docker version
New black terminal should pop-up on your laptop.
```bash
[VM @ Google - black terminal]
$ df -h .
   Filesystem      Size  Used Avail Use% Mounted on
   /dev/sda9       870G  413M  834G   1% /

$ cat /etc/*release|grep PRET
   PRETTY_NAME="Linux by CoreOS 1298.5.0 (Ladybug)"
   
$ free -g       
      total       used       free     shared    buffers     cached
Mem:    14          3         10          0          0          3
-/+ buffers/cache:          0         14
Swap:            0          0          0

$ cat /proc/cpuinfo  |grep processor|nl
     1	processor	: 0
     2	processor	: 1
     3	processor	: 2
     4	processor	: 3
     
$ docker -v
   Docker version 1.12.6, build d5236f0
```
Is it big enough for your computations? 
 See other Google VM images sizes at  https://cloud.google.com/compute/docs/machine-types

### 3) Pull compute-image (ubu-condor) from Docker into Google VM. Launch it.
Commands below are executed inside Google VM  (inside black terminal). 
We will use my Docker image w/ Ubuntu14  containing Condor job scheduler and many C++ libraries. 
  Docker pull takes about 1 minute for this 1.4GB image.
```bash
[VM @ Google - black terminal]
$ docker pull balewski/ubu14fuse-htcondor:f
    bf5d46315322: Pull complete 
    . . .
    6b08870ee12d: Pull complete 
    Status: Downloaded newer image for balewski/ubu14fuse-htcondor:f
    real    0m56.842s

$ docker images 
    balewski/ubu14fuse-htcondor f 4d7ea429fd5e 3 months ago 1.39 GB
```
 * Launch ubu14fuse-htcondor mage **inside VM, in the background**, name it 'ubuCondor'. Check it is running with 'docker ps'. 
```bash
[VM @ Google - black terminal]
$ docker run -d --name ubuCondor balewski/ubu14fuse-htcondor:f /bin/sh -c "while true; do echo hello world; sleep 1; done"

$ docker ps
    564edb5eb16b    balewski/ubu14fuse-htcondor:f ....  Up ...   ubuCondor
```
 * Connect to this 'ubuCondor' instance, verify OS is now Ubuntu 14. Note the change of the prompt. Check OS has changed.
```bash 
[VM @ Google - black terminal]
$ docker exec -it ubuCondor bash
[root@ubuCondor] 
$$ cat /etc/*release | grep PRETTY
      PRETTY_NAME="Ubuntu 14.04.5 LTS"
```
Do you see the OS is now Ubuntu14 ? If not sth went wrong - fix it before continuing.

 * You are now root inside ubu-condor instance. 
 Start condor daemon (allowing to run multiple 1-core jobs under Condor scheduler 
 even if you kill the black terminal). Check you have 4 job slots - equal to the number of v-cores you requested on VM.
```bash
[root@ubuCondor]
$$ service condor start
   Starting up Condor...    done.

$$ service condor status
   Condor is running (pid 700)

# sleep 15
$$  condor_status
Name               OpSys      Arch   State     Activity LoadAv Mem   ActvtyTime
slot1@39e6 LINUX      X86_64 Unclaimed Idle      0.200 3761  0+00:00:04
slot2@39e6 LINUX      X86_64 Unclaimed Idle      0.000 3761  0+00:00:32
slot3@39e6 LINUX      X86_64 Unclaimed Idle      0.000 3761  0+00:00:33
slot4@39e6 LINUX      X86_64 Unclaimed Idle      0.000 3761  0+00:00:34
             Total Owner Claimed Unclaimed Matched Preempting Backfill
        X86_64/LINUX     4     0       0         4       0          0        0
```

### 4) Run some computations inside 'ubu-condor'  - this is the reason you do this exercise.
 I have prepared  an example C++ code (vet3.cxx), which is very playable in terms of RAM, CPU, I/O. You will pull it from my Bitbucket.

* assumed to be a regular user. Only one exist inside this Docker image: balewski - use it as is. 

* Lets use my C++ code which does lot of computing and is well understood. Compile it. Run my vet3.exe for 1 minute.
```bash
[root@ubuCondor, black terminal]
$$ su - balewski
[balewski@ubuCondor]
$ git clone https://bitbucket.org/balewski/tutorNersc
$ cd tutorNersc/2017-03-google-gce 
$ c++ vet_v3.cxx -o vet3.exe
$ ./vet3.exe  -T 1
```
All works - I hope. If not ... do not proceed.

### 5) Verify vet3.exe runs in the background.
Start 20-minutes long vet3.exe job in the background. 
* start the vet3.exe job
```bash
[balewski@ubuCondor]
$  nohup ./vet3.exe  -T 20 >&Log1&
    [1] 1592
    
$ ~/vm-eval-tool$ ps
  PID TTY          TIME CMD
 1592 ?        00:00:08 vet3.exe
```
* now kill the black x-term. The vet3 job should continue to run. 
Check the job still runs using the web-interface.
* in the browser select 'Product & Services' (main menu), select 'Compute Engine'
* clik on the name of your VM: 'jan-4v'. Wait 1 minute and observe the CPU-load is now 25% - it is 1/4 of 4-vCores. Now we are using at last 1/4 of what we are paying for.


### 6)  Run 20 jobs on 4 vCores using scheduler.
Now we will scale up the load to use all available vCores on the VM and verify running is persistent.

* Lets enter the VM again: in the browser click on 'SSH' button (top left) - it will open new black terminal. 
Enter to [balewski@ubuCondor]. Next, kill vet3.exe at the prompt.

```bash
[VM @ Google - black terminal] 
$ docker ps
$ docker exec -it ubuCondor bash
[root@ubuCondor]
$$ su - balewski
[balewski@ubuCondor]
$ top ibn1
   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   1592 balewski  20   0  524544 514920   2776 R  99.4  3.3   5:03.21 vet3.exe

$ kill -9 1592 
$ top ibn1
```
We have cleaned up - nothing should run now.

 * Submit a batch of 20 3-minutes vet3.exe jobs, each will write continuously a 1 GB 
 output file named aaabbbNNN.out into the submit directory. 
  
 The Condor jobs defintion is in 
 the files: myJobs.condor, oneVet3.sh. 

 * **Condor scheduler** - mini tutorial
     *  condor_q  : lists all running jobs
     *  condor_rm -all  : kills all jobs
     *  condor_submit myJobs.condor : schedule a batch of jobs
     *  condor_status  : show task vs. vCore mapping
```bash
[balewski@ubuCondor]
$ cd ~/tutorNersc/2017-03-google-gce
$ ls -l oneVet3.sh  myJobs.condor
  (do you see this 2 files?)
$ condor_q   ( nothing should run)
$ condor_submit myJobs.condor
   Submitting job(s)....................
   20 job(s) submitted to cluster 8.

$ sleep 10
$ condor_q  
-- Schedd: 39e6ad94a51b : <172.17.0.2:60857?...
 ID      OWNER            SUBMITTED     RUN_TIME ST PRI SIZE CMD               
   8.0   balewski        3/10 06:49   0+00:00:14 R  0   0.0  oneVet3.sh 0
   8.1   balewski        3/10 06:49   0+00:00:14 R  0   0.0  oneVet3.sh 1
   8.2   balewski        3/10 06:49   0+00:00:14 R  0   0.0  oneVet3.sh 2
   8.3   balewski        3/10 06:49   0+00:00:14 R  0   0.0  oneVet3.sh 3
   8.4   balewski        3/10 06:49   0+00:00:00 I  0   0.0  oneVet3.sh 4
   8.5   balewski        3/10 06:49   0+00:00:00 I  0   0.0  oneVet3.sh 5
   ...
20 jobs; 0 completed, 0 removed, 16 idle, 4 running, 0 held, 0 suspended

$ condor_status 
Name               OpSys      Arch   State     Activity LoadAv Mem   ActvtyTime
slot1@39e6ad94a51b LINUX      X86_64 Claimed   Busy      1.000 3761  0+00:01:16
slot2@39e6ad94a51b LINUX      X86_64 Claimed   Busy      1.000 3761  0+00:01:17
slot3@39e6ad94a51b LINUX      X86_64 Claimed   Busy      0.990 3761  0+00:01:18
slot4@39e6ad94a51b LINUX      X86_64 Claimed   Busy      0.990 3761  0+00:01:19
                     Total Owner Claimed Unclaimed Matched Preempting Backfill
        X86_64/LINUX     4     0       4         0       0          0        0
               Total     4     0       4         0       0          0        0

$ sleep 60
$ top ibn1
top - 06:52:33 up  1:18,  0 users,  load average: 3.80, 1.88, 0.89
  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
 4626 nobody    20   0  524680 514872   2700 R  98.4  3.3   2:30.03 vet3.exe
 4633 nobody    20   0  524680 514756   2588 R  98.4  3.3   2:29.83 vet3.exe
 4637 nobody    20   0  524680 514972   2796 R  98.4  3.3   2:29.37 vet3.exe
 4641 nobody    20   0  524680 514852   2684 R  91.9  3.3   2:28.56 vet3.exe
```
Do you see 4 running and 16 idle jobs? Is the w-load close to 4.0 ?

 * Now, check again the VM load on the Browser pointed at GCE - now you will see your VM is 100% loaded. Condor runs 4 1-vCore jobs in parallel.
 * Wait for 5 minutes to see the load remains at 100%, despite the 3-minutes jobs complete. Condor is executing the next tasks from the idle queue.
 * Transfer produced output files to NERSC. Enter VM again. List and transfer (some of) output files. Note, the output file becomes visible on VM after Condor job finishes.
Note, you can transfer data from VM to any machine you like.

```bash
[balewski@ubuCondor]
$ condor_q   (wait until there is less than 20 jobs in the queue)
 $ ls -lrth /tmp/*aaabbb*
-rw-r--r--. 1 balewski balewski 997M Mar 10 06:54 /tmp/aaabbb0_959393.out
-rw-r--r--. 1 balewski balewski 997M Mar 10 06:54 /tmp/aaabbb1_959393.out
-rw-r--r--. 1 balewski balewski 997M Mar 10 06:54 /tmp/aaabbb2_959393.out
-rw-r--r--. 1 balewski balewski 997M Mar 10 06:54 /tmp/aaabbb3_959393.out
 
$ scp -rp /tmp/*aaabbb* yourName@cori.nersc.gov:$SCRATCH
aaabbb2_959393.out           100%  996MB  76.6MB/s   00:13    
aaabbb3_959393.out           100%  996MB  71.2MB/s   00:14    
aaabbb4_959393.out           100%  879MB  73.2MB/s   00:12    
```
Note, egress Google-->NERSC costs $1/10GB.
You can interupt the transfer after you see at least one file has been copied.

### 7) Shut down the VM - do not forget it - you pay for all the time it is up.
 * manipulate your VM using either web-GUI or personal/sdk-instance. The later is more reliable.

```bash
[laptop]
$ gcloud compute instances list 
NAME       ZONE   MACHINE_TYPE   PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP      STATUS
jan-4v  us-west1-a  n1-standard-4  10.138.0.2   104.196.242.169  RUNNING

$ gcloud compute instances delete jan-4v
   Do you want to continue (Y/n)?  y
 (now wait ~30 seconds)
$ gcloud compute instances list 
Listed 0 items.
```

