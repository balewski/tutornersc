## S3 bucket manipulation - GCS  (Worksheet-4 ) 
Proceed to the next step only if the outome of current step is good.

### Prerequisites
 * Account at jgi-workflows project @ Google
 * On your laptop: **personal/google-sdk** image with your  credentials against jgi-workflows project.

### 1) Create new S3 bucket at GCS
Note1, S3 bucket name are unique across GCS , no namespace.
Note2, you pay for any data residing in the S3 bucket $0.02/GB/month, incremented daily.

 * Check what S3 buckets exist at Google. Create new one named yourName-tanker33 (change the name & the int)
```bash
[laptop] 
$ docker run -it personal/google-sdk
[root@personal] 
$$ su - balewski
[balewski@personal]
$ gsutil ls
   gs://jan-bucket-a/
   gs://jan-dyb-1/
   ...
$gsutil mb  gs://jan-tanker33
   Creating gs://jan-tanker33/...

$gsutil ls
   ...
   gs://jan-tanker33/
```

### 2) Write data from laptop to S3 bucket 
* Lets create and write to this S3 bucket a small text file. Then, read it back. 'gsutil' works like a Swiss knife.
```bash 
[balewski@personal, laptop]
$ echo 'jan was here' >data1.txt

$ gsutil cp data1.txt gs://jan-tanker33
    Copying file://data1.txt [Content-Type=text/plain]...      
    Operation completed over 1 objects/13.0 B.                                        
$ gsutil ls -l gs://jan-tanker33
      13  2017-03-11T05:57:17Z  gs://jan-tanker22/data1.txt
      TOTAL: 1 objects, 13 bytes (13 B)
      
$ gsutil cp gs://jan-tanker33/data1.txt data1.txt2
    Copying gs://jan-tanker33/data1.txt...
      
$ cat  data1.txt2
jan was here
```
 Q1: can you find the  'data1.txt' file  in your S3 bucket using browser GUI?
 
 Q2: can you display content of  'data1.txt' in the web browser?
 
 * Create a 100MB file locally on your laptop and time it how long it takes to copy it to GCS (do not optimize for the number of streams for now).
```bash
[balewski@personal, laptop]
$ dd if=/dev/zero of=sample.dat bs=1M count=100
  124288000 bytes (124 MB) copied, 1.68112 s, 196 MB/s
  
$ time gsutil cp sample.dat gs://jan-tanker33
  [1 files][100.0 MiB/100.0 MiB]   12.9 MiB/s                         Operation completed over 1 objects/100.0 MiB.                             
  real	0m6.513s
```
Note, I run this when my laptop was at NERSC. At home the transfer rate is only 0.7MB/sec.

### 3) Write data from VM @ Google to S3 bucket 
 * Transfer 'sample.dat' file from GCS bucket to your VM. The same steps work. Compare data transmission
 speed - it is 4x faster to copy data from GCS to GCE. 
 
Note 1, you must run this test on the VM and not in Docker image on the VM because I have not installed 'gsutil' in ubu-condor image.
The VM will auto-install 'gsutil' at the first call.

Note 2, you can launch new black terminal attached to the same VM you own.


```bash
[VM @ Google - black terminal]
$ gsutil ls gs://jan-tanker33
gs://jan-tanker33/sample.dat

$ gsutil cp gs://jan-tanker33/data1.txt .
   Operation completed over 1 objects/13.0 B.                                  
$ cat data1.txt 
jan was here

$ time gsutil cp gs://jan-tanker33/sample.dat .
   [1 files][100.0 MiB/100.0 MiB]                                   
   Operation completed over 1 objects/100.0 MiB.
    real    0m1.830s
```

### 4) Mount S3 bucket directly to Docker instance running on  VM @ Google 

It does Work. But it requires more credentials and is not covered here.
