## Assemble genome using JGI BioBox Docker image (Worksheet-5)
Proceed to the next step only if the outome of current step is good.

### Prerequisites
 * Account at jgi-workflows project @ Google
 * On your laptop: **personal/google-sdk** image with your  credentials against jgi-workflows project.
 * BioBox 'velvet' Docker image + raw reads data 
 
### 1) Launch 2-vCore VM w/ 500 GB local disc at Google cloud. 
 * The VM launch procedure is identical as in Worksheet 3. Repeated verbatum.
 
 Remember to set a uniqe VM name by replacing 'yourName'. 
 
```bash
[laptop] 
$ docker run -it personal/google-sdk
[root@personal] 
$$ su - balewski
[balewski@personal]
$  gcloud compute instances list   # check running VMs
    Listed 0 items.
    
$ gcloud compute instances create yourName-2v  --image-project coreos-cloud --image-family coreos-stable --machine-type=n1-standard-2 --boot-disk-size=500
    (takes 10-30 seconds)
    NAME ZONE MASTER_VERSION  MASTER_IP  TYPE VERSION   NUM_NODES    STATUS
    jan-2v  us-west1-a 1.5.3  35.185.194.219  n1-standard-2  1.5.3  1 RUNNING
    
$  gcloud compute instances list   # do you see your VM?
    NAME   ZONE        MACHINE_TYPE   .... EXTERNAL_IP
    jan-2v   us-west1-a  n1-standard-2  ...    35.185.196.94 
```
 
* Connect to your VM using web interface
   * Select 'Product & Services' (main menu)
   * Select 'Compute Engine'
   * Click the 'SSH' button (far right) on the line describing the VM you have launched.
   * Optional: check disk size,  OS , RAM+swap, count vCores, and Docker version
   
New black terminal should pop-up on your laptop.
 See other Google VM images sizes at  https://cloud.google.com/compute/docs/machine-types

### 2) Assembly **draft genome** from 'raw reads' using BioBox Docker-image running on VM
This section has been prepared by Tony Wildish :
 https://bitbucket.org/TWildish/git-docker-tutorial/,
who graciously let me use it as the base for this tutorial.

This particular image (bioboxes/velvet) expects certain input data, some directory structure, and computes in a batch mode. The required setup is prepared for you in the external Bitbucket repo, separately from the bioboxes/velvet Docker image.
 * pull biobox/velvet Docker image
 * Clone input data/dir-tree from my Bitbucket
 * position yourself in bioboxes directory on the VM 
 * run computations inside biobox/velvet  Docker on the VM, mount dir-tree at Docker image launch
```bash
[VM @ Google - black terminal]
$ docker pull bioboxes/velvet
   . . .
   Status: Downloaded newer image for bioboxes/velvet:latest
   real    0m30.228s
$ git clone https://bitbucket.org/balewski/tutorNersc
$ cd tutorNersc/2017-03-google-gce/bioboxes/
bioboxes$ ls -l *
    -rwxr-xr-x. 1 balewski balewski  167 Mar 13 17:24 assemble.sh
    input_data:
       -rw-r--r--. 1 balewski balewski   125 Mar 13 17:24 biobox.yaml
       -rw-r--r--. 1 balewski balewski 27944 Mar 13 17:24 reads.fq.gz
    output_data:
    
bioboxes $ docker run --volume=`pwd`/input_data:/bbx/input:ro --volume=`pwd`/output_data:/bbx/output:rw bioboxes/velvet default
   [0.000001] Reading FastQ file /bbx/input/reads.fq.gz;
   [0.001271] 228 sequences found
   ...
   Final graph has 1 nodes and n50 of 2703, max 2703, total 2703, using 0/228 reads
   real    0m0.602s
[VM @ Google - black terminal]
```

 ### 3) check the output files and copy them back to NERSC

```bash
 [VM @ Google - black terminal, same dir as before]
 bioboxes $ ls -l output_data/
total 16
-rw-r--r--. 1 root root  108 Mar 13 17:26 biobox.yaml
-rw-r--r--. 1 root root 2812 Mar 13 17:26 contigs.fa
bioboxes $ scp -rp output_data yourName@cori.nersc.gov:$SCRATCH
  contigs.fa     100% 2812   109.5KB/s   00:00    
  biobox.yaml    100%  108     4.5KB/s   00:00    
```

### 4) Shut down the VM - do not forget it - you pay for all the time it is up.

* manipulate your VM using either web-GUI or CLI. The later is more reliable.

```bash
[laptop]
$ gcloud compute instances list 
NAME       ZONE   MACHINE_TYPE   PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP      STATUS
jan-2v  us-west1-a  n1-standard-2  10.138.0.2   104.196.242.169  RUNNING

$ gcloud compute instances delete jan-2v
   Do you want to continue (Y/n)?  y
   (now wait ~30 seconds)
$ gcloud compute instances list 
Listed 0 items.
```
