#!/bin/tcsh 
# this script produced from template on: May 09 2017 09:19AM

# sequence of steps for one r4s embedding task
# *) parse input vars 
# *) build fset-dependent sandbox using final path staring w/ ${Trigger set name}/bhla
# *) run embedding in the sandbox
# *) gzip r4s-log file 


# Note, all code in this scrpt is executed under tcsh

    set coreN = $argv[1]    
    set fSet = $argv[2]
    if ( "$argv[3]" == "" ) then 
	echo "use global NUM_EVE ="$NUM_EVE
    else 
	set NUM_EVE = $argv[3]
	echo "use task-varied NUM_EVE ="$NUM_EVE
    endif

    # those variables could have been set in the SLURM job script
    #  and need to be overwritten when templeate is processed
    set STAR_VER = SL16d
    set PATH_DAQ = /global/projecta/projectdirs/starprod/daq/2015/pp200_mtd
    set PATH_TAG = /global/projecta/projectdirs/starprod/tags/2015/pp200_mtd
        
    echo  starting new r4s PATH_DAQ=$PATH_DAQ, coreN=$coreN, execName=$EXEC_NAME, NUM_EVE=$NUM_EVE, fSet=$fSet, workerName=`hostname -f`', startDate='`date`
    echo 'pwd='`pwd` ' WRK_DIR='${WRK_DIR}  

    set daqN = $PATH_DAQ/$coreN.daq
    set tagsN = $PATH_TAG/$coreN.tags.root
    set fzdN =  $coreN.fzd
    set r4sLogFile = ${coreN}.r4s_${fSet}.r4sLog

    ls -l $daqN
    ls -l $tagsN

    cd ${WRK_DIR} # important, no clue why I  need to do it again
    pwd
    ls -l   
    ls -l   .sl64*   


    set EMYEAR = `perl StRoot/macros/embedding/getYearDayFromFile.pl -y ${coreN}`
    set EMDAY = `perl StRoot/macros/embedding/getYearDayFromFile.pl -d ${coreN}`


    # Prepare final storage dir, aka sandbox
    # for the final storage of embedding data the FSET_PATH is
    # ./${trigger set name}/${Embeded Particle}_${fSet}_${embedding requestID}/
    # The files in each fSet the REQUEST_PATH
    # ${Embeded Particle}_${fSet}_${embedding requestID}/${starProdID}.${STARLIB}/${YEAR}/${DAY}/st*
    # e.g: 
    # ls production_pp200_2015/Psi2SMuMu_104_20163401/P16id.SL16d/2015/114
    # st_mtd_adc_16114049_raw_5500008.30D2B4FE3C7A1F23_548.log.gz
    # st_mtd_adc_16114049_raw_5500008.event.root
    # st_mtd_adc_16114049_raw_5500008.geant.root


    set FSET_PATH = .//production_pp200_2015/Psi2SMuMu_${fSet}_20163401
    set REQUEST_PATH = P16id.SL16d/${EMYEAR}/${EMDAY}

    echo my FSET_PATH=$FSET_PATH
    echo my REQUEST_PATH=$REQUEST_PATH
    
    #
    # - - - -  D O   N O T  T O U C H  T H I S   S E C T I O N- - - - 
    #

    echo os-in-shifter is
    cat /etc/*release

    echo "check if in shifter (expected 1)"
    env | grep  SHIFTER_RUNTIME
    
    whoami    

    echo  load STAR enviroment 
    set NCHOS = sl64
    set SCHOS = 64
    set DECHO = 1
    set SCRATCH = $SCRATCH/out-star1
    setenv GROUP_DIR /common/star/star${SCHOS}/group/
    source $GROUP_DIR/star_cshrc.csh    
     
    echo testing STAR setup $STAR_VER in `pwd`
    starver $STAR_VER 
    env |grep STAR

    echo 'my new STAR ver='$STAR'  test root4star '
    root4star -b -q 

    #
    # - - - -   Y O U   C A N   C H A N G E   B E L O W  - - - -
    #
    echo my pwd=`pwd`
    ls -l  * .sl64*
    set sandBox = $FSET_PATH/$REQUEST_PATH
    echo check sandbox  $sandBox
    if ( -d $sandBox) then 
	echo "sandbox exist - use it"
    else
	echo "no sandbox exist - create it"
	pwd
	mkdir -p $sandBox
	ln -s ${WRK_DIR}/St*  $sandBox
	ln -s  ${WRK_DIR}/.sl64*  $sandBox
    endif
    echo step into sandbox 
    cd $sandBox
    pwd
    ls -l  * .sl64*   
    echo "===FIRE  $EXEC_NAME for coreN=$coreN fSet=$fSet "`date`

    # make sure local .cxx was copied to wrk-dir and compiled correctly in advance 
    echo start embedding for $NUM_EVE events  on $coreN r4sLogFile=$r4sLogFile
    echo full r4s log  $sandBox/$r4sLogFile

    date
    time $EXEC_NAME -b <<EOF >& $r4sLogFile
    std::vector<Int_t> triggers;
    triggers.push_back(470602);
    triggers.push_back(480602);
    triggers.push_back(490602);

    .L StRoot/macros/embedding/bfcMixer_Tpx.C
     bfcMixer_Tpx($NUM_EVE, "$daqN", "$tagsN",0, 12.0, -0.8, 0.8,  -200.0, 200.0, 100.0, 169, 8 , triggers, "P16idpp200", "FlatPt", 0, "$fzdN");
    .q
    EOF
    echo "done embedding for $coreN  "`date`
    ls -l
    gzip $r4sLogFile
    echo end of task  coreN=$coreN fSet=$fSet

