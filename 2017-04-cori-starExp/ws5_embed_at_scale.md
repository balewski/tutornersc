## SLURM embedding batch job using  taskFarmer
(proceed to the next step only if the outome of current step is good)


### Prerequisites
* NERSC credentials
* authorized to login to Cori/Edison
* added to NERSC 'repo' with a non-zero allocation, most likely: **m1094**
* this tutorial donwloaded from git as ~/tutorNersc/2017-04-cori-starExp
* sucessfully exercised the taskFarmer worksheet-4: ws4_slurm-batch-1000
* you will use/edit those files: prepEmbedTaskList.sh, starFarmer.slr,
 r4sTask_embed.templ, farmerQAutil.sh, fullJob_timeMonitor-star.ipynb
* you will machine-generate/use those files: starTask_20163401_901_906.list, r4sTask_embed.csh 
* defined specs for embedding:
  * private StRoot code (optional)
  * embeding specs, e.g.:
```bash
-trg production_pp200_2015 -production P16id -lib SL16d \
 -mixer StRoot/macros/embedding/bfcMixer_Tpx.C -prodname P16idpp200 \
 -r 20163401 \
 -geantid 169 -particle Psi2SMuMu -mode FlatPt \
 -trigger 470602 -trigger 480602 -trigger 490602 \
 -z 200.0 -vrcut 100.0 \
 -ymin -0.8 -ymax 0.8 -ptmin 0 -ptmax 12.0 \
 -mult 8 \
 -local \  (not used)
 -daq /global/projecta/projectdirs/starprod/daq/2015/pp200_mtd \
 -tag /global/projecta/projectdirs/starprod/tags/2015/pp200_mtd \
 (new flags on Cori:)
 -fSetRange 901-902 \
 -nEve 60 \
 -outPath /global/project/projectdirs/mpccc/balewski/out1 \
 -endGameOpt 
```

### Sequence of steps
 1. clone this tutorial from bitbucket , cd tutorNersc/2017-04-cori-starExp (if you have not done this earlier)

 2. edit 'prepEmbedTaskList.sh' and set the embedding specs as you wish
 
 3. prepare taskList and slurm script by executing once 'prepEmbedTaskList.sh'
 
 4. edit 'starFarmer.slr' , point 'codeDir' to your StRoot and matching .sl64_gcc482; set 'myTaskList' to the created task list
 
 5. fire one SLURM taskFarmer job 
 
 6. after SLURM job starts (may take days) monitor progress in real time using Jupyter notebook running on Cori

### 3) prepare task-scripts & SLURM job based on embeddings specs
User needs to provide TaskFarmer with the the task list and task script. Historicaly, STAR embedding helper would execute 'preparexml.sh' which converts compact embedding task description in to .xml Scheduler job description. 

Similarly  on Cori, you need to edit and execute the script 'prepEmbedTaskList.sh'  which will produce:

 * lost of task for the taskFarmer, one line per daq file per fset 'starTask_20163401_901_906.list'
 * the r4s task script 'r4sTask_embed.csh' needed to run  the embedding on one daq file. 

```bash
ssh cori
module load python/3.5-anaconda 
./prepEmbedTaskList.sh  
Done  r4sTask_embed.csh
. . . 
output: starTask_20163401_901_906.list
. . . 
```

#### 3.1) Task list 
It will consist of concatenated, randomized N fsets. The name will be
starTask_{embed request id}_{fset1}_{fset2}.list, e.g.:
starTask_20163401_101_104.list
Note, if -endGameOpt is used, 3 more FSETs are added to this task list.
You can change it in  ./prepEmbedTaskList.sh , in  makeTaskList(argD,taskLF), variable outF.


E.g. the example file in git 'starTask_20163401_901_906.list' was produced with
```bash
 -r 20163401 \
 -daq /global/projecta/projectdirs/starprod/daq/2015/pp200_mtd \
 -fSetRange 901-902 \
 -nEve 150 \
 -endGameOpt \

output: starTask_20163401_901_906.list
---make fset= 901  nEve= 150
---make fset= 902  nEve= 150
---make fset= 903  nEve= 75
---make fset= 904  nEve= 37
---make fset= 905  nEve= 18
total len of task list is 2900
base daq list len 580
```
Or w/o -endGameOpt the output will be:
```bash
output: starTask_20163401_901_903.list
---make fset= 901  nEve= 150
---make fset= 902  nEve= 150
total len of task list is 1160
base daq list len 580
```

#### 3.2) r4s task script 'r4sTask_embed.csh' 

It has fixed name as above. It is produced from 'r4sTask_embed.templ' - edit **only** the templet file and regenarate 'r4sTask_embed.csh' by re-running ./prepEmbedTaskList.sh , as needed.

TaskFarmer expects a list of 1-vCore serial jobs with fully defined list of input parameters changing with each task, one task per line, no wilde cards, no formulas.
It is up to user to decide what operation will be performed by 'r4sTask_embed.csh' 1-vCore task based on given 3 parameters.

### 4) Customize taskFarmer SLURM job script 'starFarmer.slr'

The complete SLURM job script capable to run STAR analysis using a Shifter image is in the file: 'starFarmer.slr'. See other worksheet (ws4_slurm-batch-1000) for the details.

You may verify the following variables are reasonable :
```bash
#SBATCH -N 3 -c64 -t 29:00
export SKEW=50 
export THREADS=50 (or 40 for Edison)
myTaskList=starTask_20163401_901_906.list
myTaskCSH=r4sTask_embed.csh
export STAR_VER=SL16d
export NUM_EVE=8 
export PATH_DAQ=....
codeDir=....
```


### 5) Fire SLURM job w/ taskFarmer.
To submit the SLURM job execute:
```bash
cori07> sqs
cori07> sbatch starFarmer.slr 
Submitted batch job 4605404

cori07> sqs
JOBID              ST   REASON       USER         NAME         NODES        USED         REQUESTED    SUBMIT                PARTITION    RANK_P 
4605404            PD   None         balewski     starFarm-dbg 3            0:00         29:00        2017-04-17T14:04:00   debug        1604 

```

If all goes well you will see several log files:
#### 5.a) slurm log file
In the submit directory, named: **slurm-4605404.out**. Check there firts for potential problems.

#### 5.b)  job sandbox, taskFarmer logs per job
All other files will show up here: **$SCRATCH/starFarm-4605432/logs/** . Spotcheck one task , e.g.  st_mtd_adc_16116041_raw_5500010_fset500.taskLog

#### 5.c)  BFC logs are stored at  -outPath 
All other files will show up here: **$SCRATCH/starFarm-4605432//production_pp200_2015/169_500_20163401/169_500_20163401/P16id.SL16d/2015/116/** . Spotcheck BFC log files , e.g.  st_mtd_adc_16116041_raw_5500010.r4s_500.r4sLog

### 6) SLURM job monitoring

Before taskFarmer starts to to the work for you a monitoring bash script is executed in the background (farmerQAutil.sh ). It updates a JSON file ( mon.farmer-4605404) in the submit directory w/ # of running & completted BFC jobs as well as # of openned DB connections. The later tend to slow down start of BFC  at cold-start  up to 40 minutes.

You can plot progress of the taskFarmer job using Jupiter notbook fired at Cori. 

 * Point your browser to :
https://jupyter-dev.nersc.gov 

 * Open provided notbook  'fullJob_timeMonitor-star.ipynb'  inside Jupyter (browse for it)
 * Change jobId in variable logF='farmer.mon-4605502' 
 *  monitor SLURM job  progress by peroidically pressing tab 'Cell --> Run All'.


### 7) Ramp it up
Change # of events and number of fsets in ./prepEmbedTaskList.sh , re-run this script.

Change the following variables to be :
```bash
#SBATCH -N 21 -c64 -t 16:01:00
export SKEW=2500 
export THREADS=50
myTaskList=starTask_NEW-LIST_NAME.list
```
Note, you can request the 'premium' QOS  which will significantly  reduce your queue waiting time, but will cost double allocation.


### 8)  More detailed monitoring 
You can use  r4sTaskEval.py r4sPerfQA_slurm.ipynb
to produce per-task evaluation JSON file (starQA1.json) and make more detailed plots (see google doc)

