# README #

Cori tutorials for STAR users

### What is this repository for? ###

Supporintg slides for this hands-on tutorial are in [Google Doc](https://docs.google.com/presentation/d/1MIioQWQXz99Ijk7gUo5HtTzVrno8XRdEJ0f7Q7bSXHw)
### How do I get set up? ###

User needs NERSC credentials, be authorized to login to Cori/Edison, be added to a repo with non-zero allocation

### What will I learn? 
Open worksheets (.md files)  in the browser, titles are self-explanatory 

 * ws1_bfc-interactive- easy
 * ws2_slurm-batch-1 - academic example for one 1vCore root4star task 
 * ws3_slurm-batch-1000 - large scale job submission (1000s concurent tasks)
 * ws4_slurm-batch-50 - small scale, 50 concurent tasks on one node 
 * ws5_embed_at_scale - specialized , for STAR embedding helper
 * ws9_FAQ.md  - typical errors/problems you may encounter

Bonus:  Mpi-Hallo-World.md - A simple example testing multi-node MPI works on Cori using custom Docker image (Ubuntu 16)
