### MPI Hello-world on Cori 

A simple example testing multi-node MPI works on Cori using custom Docker image (Ubuntu 16)

```bash
ssh cori

salloc --nodes=2  --partition=regular --qos=interactive --time=00:30:00 --account=nstaff  -C haswell --image=docker:balewski/ubu16-py27-mpich32:v1

git clone https://balewski@bitbucket.org/balewski/stonoga-mpi.git 
cd stonoga-mpi/basic/ 
mpic++ -o mpi_hello.ex mpi-hello-v3.cxx 
exit 

# now you are out of shifter but still on interactive node: 

balewski@nid00010:~/janTest> srun -n4 shifter ~/janTest/stonoga-mpi/basic/mpi_hello.ex 
Hello world from processor nid00010, rank 0 out of 4 processors 
Hello world from processor nid00010, rank 1 out of 4 processors 
Hello world from processor nid00030, rank 3 out of 4 processors 
Hello world from processor nid00030, rank 2 out of 4 processors 
```

