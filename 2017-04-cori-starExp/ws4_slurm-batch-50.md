## Small scale SLURM batch job using **jobPump** (worksheet 4)
(proceed to the next step only if the outome of current step is good)


### Prerequisites
* NERSC credentials
* authorized to login to Cori/Edison
* added to NERSC 'repo' with a non-zero allocation, most likely: **m1094**
* this tutorial donwloaded from git as ~/tutorNersc/2017-04-cori-starExp
* sucessfully exercised  worksheet-2: ws2_slurm-batch-1 and 
the taskFarmer worksheet-3: ws3_slurm-batch-1000

* you will use/edit those files: starPump.slr, r4sTask_bfc.csh , starUtilPump.sh, fullJob_timeMonitor-star.ipynb
* you will use this task list: starTask_600.list 


### Sequence of steps
 1. clone this tutorial from bitbucket , cd tutorNersc/2017-04-cori-starExp (if you have not done this earlier)

 2. edit 'starPump.slr' , point 'codeDir' to your StRoot and matching .sl64_gcc482; set 'myTaskList' to the created task list
 
 3. fire one SLURM  jobPump job 
 

### 2) Customize jobPump SLURM job script 'starPump.slr'

The complete SLURM job script capable to run STAR analysis using a Shifter image is in the file: 'starPump.slr'. See other worksheet (ws4_slurm-batch-1000) for the details.

You may verify the following variables are reasonable :
```bash
#SBATCH -N 1 -c64 -t 29:00
export STAR_VER=SL16d
export EXEC_NAME=root4star

export MAX_JOBS_LOAD=$[ $SLURM_CPUS_ON_NODE -4 ]
export MAX_JOBS_LOAD=60  
export MAX_W_LOAD=$[ $MAX_JOBS_LOAD +2 ]
#export TOT_JOB_LIMIT=31  # optional cut-off to task list

export NUM_EVE=8 # warn, may be changed in star-.csh task
myTaskList=starTask_600.list
myTaskCSH=r4sTask_bfc.csh
export PATH_DAQ=....
codeDir=....
```


### 3) Fire SLURM job w/ jobPump.
To submit the SLURM job execute:
```bash
cori07> sqs
cori07> sbatch starPump.slr 
   Submitted batch job 4605404
```

If all goes well you will see several log files:
#### 3.a) slurm log file
In the submit directory, named: **slurm-4605404.out**. Check there firts for potential problems.

#### 3.b)  job sandbox, jobPump log 
One file for all tasks will show up here: **$SCRATCH/starPump-4605432/pump.log** . 

#### 3.c)  BFC logs are stored at  -outPath 
All other files will show up here: **$SCRATCH/starPump-4605432/logs/** . Spotcheck BFC log files , e.g.  st_mtd_adc_16116041_raw_5500010.r4s_500.r4sLog


