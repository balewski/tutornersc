#!/bin/csh
set coreN = "st_mtd_adc_16058077_raw_1000009"
set inpPath = "/global/projecta/projectdirs/starprod/daq/2015/pp200_mtd/"
set daqF  = $inpPath$coreN".daq"
set tagsF = $inpPath$coreN".tags.root"
set fzdF =  $coreN".fzd"
set nEve = 2
ls -l $daqF
ls -l $tagsF

starver SL16d
echo $STAR
root4star -b -q 

echo start embedding for $nEve events  on $coreN

date
time root4star -b <<EOF
  std::vector<Int_t> triggers;
  triggers.push_back(470602);
  triggers.push_back(480602);
  triggers.push_back(490602);
  .L StRoot/macros/embedding/bfcMixer_Tpx.C
  bfcMixer_Tpx($nEve, "$daqF", "$tagsF", 0, 12.0, -0.8, 0.8, -200.0, 200.0, 100.0, 169, 8, triggers, "P16idpp200", "FlatPt", 0, "$fzdF");
  .q
EOF
echo embedding done
date
ls -l
