## Interactive root4star  Cori session  w/ STAR Shifter image
### Worksheet-1 , tested on 2017-12-06 
(proceed to the next step only if the outome of current step is good)


### Prerequisites
* NERSC credentials
* authorized to login to Cori/Edison
* added to NERSC 'repo' with a non-zero allocation, most likely: **m1094**
* this tutorial donwloaded from git as ~/tutorNersc/2017-04-cori-starExp

### 1) Login to Cori
* clone tutorial from git
```bash
[laptop] 
$ ssh  -X cori.nersc.gov
[cori-nn] # login node is different from Cori worker node
$ git clone https://bitbucket.org/balewski/tutorNersc
$ cd tutorNersc/2017-04-cori-starExp  
$ ls  # do it just once, to see the files
```
 
### 2) Launch STAR image as interactive session on 64-vCore worker node
 Note, this is an overkill, since you will use just 1 vCore out of 64 you pay for, but you will not wait long for the session to start.
 
* verify the STAR Shifter image **pdsf-sl64-star:v3** exist
* *salloc* launches interactive session for specified duration. The short form of arguments exist
* count vCores, OS-version for worker node - OS is not what you need
* check access to your files, $CSCRATCH, /project , /projecta

```bash
[cori-nn]
$  shifterimg images | grep star      
		cori  custom     READY  2017-02-10T12:54:09 pdsf-sl64-star:v7
# there will be many version of STAR shofter image , pick the latest

# a) you can get the whole node: costs 80 mpph/wall hour, starts faster
$  salloc --nodes 1 --partition regular --qos=interactive -C haswell --time 45   
	salloc: Pending job allocation 4407992
	salloc: job 4407992 queued and waiting for resources
	salloc: job 4407992 has been allocated resources
	salloc: Granted job allocation 4407992
	salloc: Waiting for resource configuration
	salloc: Nodes nid00011 are ready for job

# b) you can get the just 2 cores, cost whole node: costs 2.5 mpph/wall hour,  may take long to start - but you never know for sure
$  salloc --ntasks 2 --partition debug -C haswell --time 29 

[balewski@nid00011] # now you are on a worker node (for 45 minutes)
$ lstopo -c | grep PU | nl   # verify you got 64 vCores
$ cat /etc/*release |grep PRETTY
    PRETTY_NAME="SUSE Linux Enterprise Server 12"
# it will be NOT Scientific Linux you need for STAR
$ ls ~/
$ ls  $SCRATCH
$ ls  /project/projectdirs/star
$ ls /global/projecta/projectdirs/starprod/daq/2015/pp200_mtd/
# all ls above should work
$ echo inShifter:`env|grep  SHIFTER_RUNTIME`
# should return nothing, because Shifter was not activated yet
```
Now the STAR-related  operations:

*  clear environment variables (needed  for the  change  from bash to tcsh)
*  change cgroup, this will jump in to shifter image, OS will be SL6
*  load STAR environment
```bash
# copy/past this block of 'unset'   
 unset MODULE_VERSION_STACK
 unset MODULE_VERSION
 unset MODULEPATH MODULESHOME
 unset LOADEDMODULES PRGENVMODULES

# change cgroup, VERY IMPORTANT:
$ shifter --image=custom:pdsf-sl64-star:v7  /bin/tcsh  
$ echo inShifter:`env|grep  SHIFTER_RUNTIME`
# now the sys-var should be set - you are in Shifter cgroup - if not it is fatal
$ echo inShifter:`env|grep  SHIFTER_RUNTIME`
    inShifter:SHIFTER_RUNTIME=1

$ cat /etc/*release  # check OS has changed , now OS is good for STAR
    Scientific Linux release 6.4 (Carbon)

$ load STAR enviroment (hack 15-years old STAR legacy setup), copy/paste this block
set NCHOS = sl64
set SCHOS = 64
set DECHO = 1
set SCRATCH = $CSCRATCH
setenv GROUP_DIR /common/star/star${SCHOS}/group/
source $GROUP_DIR/star_cshrc.csh
```
Verify STAR software is avaliable:
```bash
# verify what is the default STAR environment
$ echo $STAR
  /common/star/star64/packages/SL15e
  
$ root4star -b -q
  *******************************************
  *                                         *
  *        W E L C O M E  to  R O O T       *
  *                                         *
  *   Version   5.34/09      26 June 2013   *
  *                                         *
  *  You are welcome to visit our Web site  *
  *          http://root.cern.ch            *
  *                                         *
  *******************************************

ROOT 5.34/09 (v5-34-09@v5-34-09, Jun 26 2013, 17:10:36 on linux)

CINT/ROOT C/C++ Interpreter version 5.18.00, July 2, 2010
Type ? for help. Commands must be C++ statements.
Enclose multiple statements between { }.
*** Float Point Exception is OFF ***
 *** Start at Date : Tue Apr  4 12:06:53 2017
QAInfo:You are using STAR_LEVEL : pro, ROOT_LEVEL : 5.34.09_1 and node : nid00015 
```
Run short starsim simulation, following STAR drupal instruction https://drupal.star.bnl.gov/STAR/comp/simu/event-gen

```bash
$ln -s $STAR/StRoot/StarGenerator/macros/starsim.pythia8.C starsim.C
$ time root4star -q -b starsim.C\(10\)

# display tracks Pz
$ root4star -l
root [0] TFile::Open("pythia8.starsim.root")
root [1] genevents->StartViewer()
root [2] genevents->Draw("mPz","mStatus>0")

```
Compile your private code w/ cons in SL16d
```bash
$ starver SL16d
$ echo $STAR
  /common/star/star64/packages/SL16d
$ cons
  cons: nothing to be built in ".".
```

### 3) Run STAR BFC on few au-au events
* check you are in SL64 inside STAR shifter image
* find the .daq files location
* change working dir to scratch on Cori
* fire root4star
* inspect produced muDst

```bash
[balewski@nid00011]
$  cat /etc/*release
   Scientific Linux release 6.4 (Carbon)
$ starver SL16d 
$ echo $STAR
   /common/star/star64/packages/SL16d

$ ls -l /global/projecta/projectdirs/starprod/daq/2015/pau200_bht1_adc/st_physics_adc_16150045_raw_5500055.daq

$ mkdir $SCRATCH/testJan
$ cd $SCRATCH/testJan

# fire BFC on 5 events in a scratch area
$ time root4star -b -q bfc.C'(1,5,"DbV20160418 P2014a pxlHit istHit btof mtd mtdCalib BEmcChkStat -evout CorrX OSpaceZ2 OGridLeak3D -hitfilt", "/global/projecta/projectdirs/starprod/daq/2015/pau200_bht1_adc/st_physics_adc_16150045_raw_5500055.daq")' > & Log1 &

    Done after: 193.668u 1.176s 3:32.15 91.8%	0+0k 54324+0io 164pf+0w
    BFC connected to  :
    St_db_Maker:INFO  - MysqlDb::Connect(host,user,pw,database,port) line=525  Server Connecting: DB=StarDb  Host=mstardb02.nersc.gov:3316

$ ls -lrt  #verify the *.root output file are there

# browse muDst 
$ root4star -l
root4star [0] TFile::Open("st_physics_adc_16150045_raw_5500055.MuDst.root")
root4star [1] MuDst->StartViewer()

```
 
### 4) Shut down the interactive session


```bash
[balewski@nid00011]
$ exit
[cori-nn]
$ exit
[laptop]

```
### 5) Appendix 1 : run jobPump interactively
You need to launch interactive SLURM job with salloc, set minimal number of variables by hand and fire the jobPump. Do NOT enter shifter when launching jobPump.

```bash
[cor12] 
$  salloc --nodes 1 --partition regular --qos=interactive -C haswell --time 45   --image=custom:pdsf-sl64-star:v6

[@nid00011]
whoami
env|grep  SHIFTER_RUNTIME

unset MODULE_VERSION_STACK
unset MODULE_VERSION
unset MODULEPATH MODULESHOME
unset LOADEDMODULES PRGENVMODULES

cd tutorNersc/2017-04-cori-starExp/

export WRK_DIR=~/tmp
mkdir -p $WRK_DIR/logs
cp r4sTask_bfc.csh $WRK_DIR

export MAX_JOBS_LOAD=60 
export MAX_W_LOAD=60
export TOT_JOB_LIMIT=20

export NUM_EVE=8
export EXEC_NAME=root4star
export STAR_VER=SL16d
export PATH_DAQ=/global/projecta/projectdirs/starprod/daq/2015/pp200_mtd/

# now  the environment is set up, start/kill the command below at will
./starUtilPump.sh starTask_600.list >& $WRK_DIR/pump.log8
```

