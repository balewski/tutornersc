## Large scale SLURM batch job on many Cori nodes using **taskFarmer**
### Worksheet-3
(proceed to the next step only if the outome of current step is good)


### Prerequisites
* NERSC credentials
* authorized to login to Cori/Edison
* added to NERSC 'repo' with a non-zero allocation, most likely: **m1094**
* this tutorial donwloaded from git as ~/tutorNersc/2017-04-cori-starExp

### 1) Login to Cori
Clone this tutorial from bitbucket (if you have not done this earlier)
```bash
[laptop] 
$ ssh  -X cori.nersc.gov
[cori-nn] # note, Cori login node is different from a Cori worker node
$ git clone https://bitbucket.org/balewski/tutorNersc
$ cd tutorNersc/2017-04-cori-starExp  
$ ls  # you will need those files:
 pp200_mtd.csv prepTaskList.sh r4sTask_bfc.csh  starFarmer.slr starTask_600.list
fullJob_timeMonitor-star.ipynb  r4sTaskEval.py r4sPerfQA_slurm.ipynb 
```
 
For this exercise you will use the [taskFarmer](http://www.nersc.gov/users/data-analytics/workflow-tools/taskfarmer/) MPI tool to orchiestrate parallel and serial execution of large number of 1-vCore root4star (r4s) tasks.
The general layout of a SLURM job w/ Shifter image is as follows. 

1. given a plain text run list (pp200_mtd.csv) prepare ready to execute taskFarmer tasks list (starTask_600.list)
2. fire SLURM job,  feed it w/ the tasks list
3. after SLURM job  startes (may take days) monitor progress in real time using Jupyter notebook running on Cori

### 2) Prepare taskFarmer input

TaskFarmer expects a list of 1-vCore serial jobs with fully defined list of input parameters changing with each task, one task per line, no wilde cards, no formulas. TaskFarmer will work on this list sequential. At the cold-start taskFarmer will push the requested number of 1-vcore tasks per node (THREADS)  to all nodes (except of the head node). Next, TaskFarmer  will periodically monitor if all tasks are still running and as needed it will fire new tasks from the list to keep all nodes  fully loaded. After the task list is exhausted and all running tasks complete (or crash) taskFarmer will cancell the SLURM job.

Here is an example of a taskFarmer input:
```bash
$ cat vegiList5 
onePotato.sh a b
onePotato.sh 9 x
oneTomato.sh  red aa
onePotato.sh m f
oneTomato.sh  green bb
onePotato.sh 33 44
```
It is up to user to decide what operation will be performed by 'onePotato.sh' 1-vCore task based on given 2 parameters.

Let assume STAR user wants to run BFC on daq files. The BFC chain is defined in the script 'r4sTask_bfc.csh' (equivalent to  'onePotato.sh' mentioned above).
```bash
$ cat r4sTask_bfc.csh
#!/bin/tcsh 
 set coreN = $argv[1]
 bfcString="DbV20160418,P2014a,pxlHit,istHit,btof,mtd,mtdCalib,BEmcChkStat,-evout,CorrX,OSpaceZ2,OGridLeak3D,-hitfilt"

 set daqN=$PATH_DAQ/$coreN.daq
 set coreOut=${coreN}_n$NUM_EVE
    
 # - - - -  D O   N O T  T O U C H  T H I S   S E C T I O N- - - - 
  cd ${WRK_DIR} # important
  <snip> do not change this section of script
 # - - - -   Y O U   C A N   C H A N G E   B E L O W  - - - -
    
 echo "fire $EXEC_NAME for coreN=$coreN "`date`
 time $EXEC_NAME -b -q bfc.C\($NUM_EVE,\"$bfcString\",\"$daqN\"\)
 echo "completed job $coreN   "`date` 
```
This r4sTask_bfc.csh script takes 1 run-time arguments changing w/ each r4s task:  the name of daq file. The constant bfcString and 3 system variables $NUM_EVE, $WRK_DIR, $PATH_DAQ are not changing w/ r4s task  and need to be defined as well (e.g. in the .slr script).

Below is the taskFarmer task list for a STAR BFC jobs, matching the above execution task (r4sTask_bfc.csh):
```bash
$  head starTask_600.list -n 5
shifter  /bin/tcsh  ${WRK_DIR}/r4sTask_bfc.csh st_mtd_adc_16114048_raw_2500009 >&   ${WRK_DIR}/logs/st_mtd_adc_16114048_raw_2500009.r4sLog
shifter  /bin/tcsh  ${WRK_DIR}/r4sTask_bfc.csh st_mtd_adc_16059022_raw_2000006 >&   ${WRK_DIR}/logs/st_mtd_adc_16059022_raw_2000006.r4sLog
shifter  /bin/tcsh  ${WRK_DIR}/r4sTask_bfc.csh st_mtd_adc_16085055_raw_5500005 >&   ${WRK_DIR}/logs/st_mtd_adc_16085055_raw_5500005.r4sLog
shifter  /bin/tcsh  ${WRK_DIR}/r4sTask_bfc.csh st_mtd_adc_16105038_raw_2500006 >&   ${WRK_DIR}/logs/st_mtd_adc_16105038_raw_2500006.r4sLog
shifter  /bin/tcsh  ${WRK_DIR}/r4sTask_bfc.csh st_mtd_adc_16073013_raw_1500005 >&   ${WRK_DIR}/logs/st_mtd_adc_16073013_raw_1500005.r4sLog
```

It was my choice to arrange the working directory  ${WRK_DIR}  will be a sand-box created by the SLURM job on $SCRATCH disc and contain SLURM jobID as part of the name - you can make different arrangements.


Assuming a STAR user has a simple list of daq files + number of events
```bash
$head pp200_mtd.csv
st_mtd_adc_16114048_raw_2500009.daq	409
st_mtd_adc_16059022_raw_2000006.daq	220
st_mtd_adc_16085055_raw_5500005.daq	485
st_mtd_adc_16105038_raw_2500006.daq	551
st_mtd_adc_16073013_raw_1500005.daq	181
```
It needs to be converted into starTask_600.list with the script prepTaskList.sh
```bash 
$ ./prepTaskList.sh pp200_mtd.csv r4sTask_bfc.csh starTask_600.list-xxx
```
Note, the 'starTask_600.list' is already provided, so here the output name has appended '-xxx'.

### 3) Customize taskFarmer SLURM job script 'starFarmer.slr'

The complete SLURM job script capable to run STAR analysis using a Shifter image is in the file: 'starFarmer.slr'.
This script was optimized to run even 1000 paralell r4star jobs on 21 nodes for 1.5 days using external STAR DB at BNL.
Below I'll discuss only the variables relevant for running optimal at Cori on Haswell. It is up to your curiosity to figure out why so many other variables and operations are needed - they are needed.

#### 3.a) SLURM job definition for Cori 
**(see google-doc how to change it for Edison)**

We will start with a 3-node SLURM job running for up to 30 minutes in the debug partition (aka queue) - it will start quickly. We want to use the Shiftert image as specified.  
```bash
#SBATCH -N 3  # number of nodes
#SBATCH -c64 -C haswell # 123 GB RAM
#SBATCH --partition debug -t 00:29:00   -J starFarm-dbg
#SBATCH --image=custom:pdsf-sl64-star:v3
```

#### 3.b) taskFarmer params
```bash
export SKEW=60  # (sec) stagger first round of task farmer tasks
export THREADS=50 # how many jobs per node , can be less than '-c'value but not more
myTaskList=starTask_600.list
myTaskCSH=r4sTask_embed.csh
wrkDir=$SCRATCH/starFarm-$SLURM_JOBID
```

#### 3.c) STAR task parameters
```bash
export STAR_VER=SL16d
export NUM_EVE=8
export WRK_DIR=$wrkDir
export PATH_DAQ=....
codeDir=.... (private StRoot, etc)
```
It takes about 10 minutes to process 8 STAR events using BFC from provided example. The processing of the 1st event make take up to 5 minutes depending on the DB load.

### 4) Fire SLURM job w/ taskFarmer.
To submit the SLURM job execute:
```bash
cori07> sqs
cori07> sbatch starFarmer.slr 
Submitted batch job 4605404

cori07> sqs
JOBID              ST   REASON       USER         NAME         NODES        USED         REQUESTED    SUBMIT                PARTITION    RANK_P 
4605404            PD   None         balewski     starFarm-dbg 3            0:00         29:00        2017-04-17T14:04:00   debug        1604 

```

After some time (minutes to hours) your job will enter in to execution - periodically repeat sqs to check the status.
It should launch (N-1)*THREADS=100 root4star tasks, spreaded evenly on 2  out of 3 nodes.

If all goes well you will see several log files:
#### 4.a) slurm log file
In the submit directory, named: **slurm-4605404.out**. Check there firts for potential problems.

#### 4.b)  job sandbox, BFC logs
All other files will show up here: **$SCRATCH/starFarm-4605432/** . Spotcheck BFC log files , e.g.  st_mtd_adc_16108033_raw_2000009.log.

### 5) SLURM job monitoring

Before taskFarmer starts to to the work for you a monitoring bash script is executed in the background (farmerQAutil.sh ). It updates a JSON file ( mon.farmer-4605404) in the submit directory w/ # of running & completted BFC jobs as well as # of openned DB connections. The later tend to slow down start of BFC  at cold-start  bu ypto 40 minutes.

You can plot progress of the taskFarmer job using Jupiter notbook fired at Cori. 

 * Point your browser to :
https://jupyter-dev.nersc.gov 

 * Open provided notbook  'fullJob_timeMonitor-star.ipynb'  inside Jupyter (browse for it)
 * Change jobId in variable logF='farmer.mon-4605502' 
 *  monitor SLURM job  progress by peroidically pressing tab 'Cell --> Run All'.
After ~10 minutes the red dots showing finished jobs should rise up. After 30 minutes SLURM will kill your job.

Now you can evaluate muDst for completed BFC tasks.

To gain more insight in to DB load, you may  watch one of them via Ganglia monitoring. Point your browser at (mstardb02)
[http://128.55.160.65/ganglia/?r=hour&cs=&ce=&m=load_one&c=PDSF+Batch&h=mstardb02.nersc.gov&tab=m&vn=&hide-hf=false&mc=2&z=small&metric_group=ALLGROUPS]


### 6) Ramp it up
Change the following variables to be :
```bash
#SBATCH -N 21 -c64 -t 16:01:00
export SKEW=2500 
export THREADS=50
export NUM_EVE=100
```
Increase task list length to 6000 lines, fire the job and wait.

Note, you can request the 'premium' QOS  which will significantly  reduce your queue waiting time, but will cost double allocation.


### 7)  More detailed monitoring 
You can use  r4sTaskEval.py r4sPerfQA_slurm.ipynb
to produce per-task evaluation JSON file (starQA1.json) and make more detailed plots (see google doc)

