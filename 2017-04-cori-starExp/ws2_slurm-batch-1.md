## Tiny SLURM batch job: 1 root4star on a fraction of a node 
### Worksheet-2
(proceed to the next step only if the outome of current step is good)

### Prerequisites
* NERSC credentials
* authorized to login to Cori/Edison
* added to NERSC 'repo' with a non-zero allocation, most likely: **m1094**
* this tutorial donwloaded from git as ~/tutorNersc/2017-04-cori-starExp

### 1) Login to Cori
* clone tutorial from git (if you have not done this earlier)
```bash
[laptop] 
$ ssh  -X cori.nersc.gov
[cori-nn] # login node is different from Cori worker node
$ git clone https://bitbucket.org/balewski/tutorNersc
$ cd tutorNersc/2017-04-cori-starExp  
$ ls  #  # you will need those files:
 hello.slr starOne.slr r4sTask_bfc.csh 
```
 
### 2) Submit hello-world SLURM job to Cori
 
* inspect job description script *hello.slr*. It consists of SLURM commands starting with '#SBATCH bhla' which take no parameters, next there are regular bash commands executed on the head node of the job. This set of exercises will not launch jobs on more than 1 node, so this distinction is moot.

```bash
[cori-nn]
$  cat hello.slr
#!/bin/bash -l
#SBATCH --nodes=1 --time=00:02:00
# some comment
#-SBATCH this is also a comment line
#SBATCH --partition=debug
#SBATCH --constraint=haswell
echo  hello-world, see vCores=$SLURM_CPUS_ON_NODE
env | grep SLURM
```


* fire SLURM job : **sbatch [jobScript]**
* monitor job progress : **sqs**
* check history of accepted jobs (failed or successfull) : **sacct -j [jobID]**
* inspect job output : **slurm-[jobID].out**


```bash
cori08>  hello.slr
Submitted batch job 4411338

cori08> sqs
JOBID             USER         NAME         NODES        USED         REQUESTED     PARTITION    
4411338          balewski     hello.slr    1            0:03         	2:00         debug 
                     
cori08:> sacct -j 4411338
       JobID    JobName  Partition    Account  AllocCPUS      State ExitCode 
------------ ---------- ---------- ---------- ---------- ---------- -------- 
4411338      hello.s+      debug     nstaff         64  COMPLETED      0:0 
4411338.bat+      batch              nstaff         64  COMPLETED      0:0 
4411338.ext+     extern              nstaff         64  COMPLETED      0:0 
```
This is the output of the hello.slr job
```bash
cori08> cat slurm-4411338.out 

Hello-world, see vCores=64
SLURM_JOB_NAME=hello.slr
SLURMD_NODENAME=nid00115
SLURM_JOB_QOS=debug
SLURM_JOB_ID=4411338
SLURM_SUBMIT_DIR=../2017-04-cori-starExp
SLURM_CPUS_ON_NODE=64
SLURM_JOB_ACCOUNT=nstaff
SLURM_JOB_NUM_NODES=1
SLURM_MEM_PER_NODE=124928
. . . 
```


### 3) Run root4star BFC, one task on 2-vCores job-slot in  the 'shared' queue
This is still an academic example, not suited for scaling to 10,000 root4star tasks.
There are more details about .slr and .csh scripts in the next worksheet3.

The general layout of a SLURM job w/ Shifter image is as follows. There are 2 scripts:

* slurm-job description (starOne.slr) executed in the naitive OS(SUSE), invoking 'shifter' command
* user script (r4sTask_bfc.csh) invoked in shifter environment. Task jumps into the image w/ OS : SL6+STAR libs

To use allocation (mpp hours) frugally we will submit this 1-core app (STAR BFC) to the **shared partition** on Cori which will give us only a 1/32 fraction of the Haswell node.
Since BFC may need above 1.8 GB of RAM we will ask for 2-vCore SLURM job-slot, which will give us 2*1.8=3.6 GB of RAM. Feel free to play w/ '--ntasks'.

Before you fire SLURM job

* check what SLURM jobs you are already running (sqs)? Is the input daq file accessible?
* inspect both scripts: starOne.slr, r4sTask_bfc.csh. Recognize which commands are executed in which OS. 

TRICKS: 

  * Trick 1: the '# eve' value is passed between scripts using system variable (use bash/tcsh syntax appropriately).
  * Trick 2: BFC runs in a sandbox created by r4sOne.slr on $SCRATCH, the sandbox name include SLURM job ID so mutiple SLURM jobs will not collide.
  * Trick 3 : copy private StRoot & .sl64_gcc482 into the sandbox before you jump
  * Trick 4 :  to help debugging  job output is split:
      *  $WRK_DIR/logs/ - root4star output
	  *   slurm-jobID.out  - anything else printed by both scripts (.csh, .slr) 
* submit the job to slurm - it may take minutes to hours untill it starts, depending on the competition on Cori. After job starts it will run for 5-10 minutes. 
* inspect the output files
  
Note, the more resources your job needs (# of vCores, duration) the longer you will wait for this job start
```bash
cori08>sqs
cori08>ls -l    /global/projecta/projectdirs/starprod/daq/2015/pau200_bht1_adc/st_physics_adc_16150045_raw_5500055.daq

cori08>sbatch bfcOne.slr 
   Submitted batch job 4434052

cori08>sqs

cori08> sacct -j 4434458
       JobID    JobName  Partition    Account  AllocCPUS      State ExitCode 
------------ ---------- ---------- ---------- ---------- ---------- -------- 
4434458      bfcOne.slr      debug     nstaff         64  COMPLETED      0:0 
4434458.bat+      batch                nstaff         64  COMPLETED      0:0 
4434458.ext+     extern                nstaff         64  COMPLETED      0:0 

cori08> vi  $SCRATCH/starFarm-8854403/logs/st_mtd_adc_16114048_raw_2500009.r4sLog

QA :INFO  - QAInfo:Run is finished at Date/Time 20170405/131135; 
  Total events processed :5 and not completed: 0
QA :INFO  - QAInfo:Chain            StBFChain::bfc                 
  Ast =154.99        Cpu =149.94

```
 