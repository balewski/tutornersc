#!/bin/tcsh 
# Note, all code in this scrpt is executed under tcsh

    set coreN = $argv[1]
     
    set bfcString="DbV20160418,P2014a,pxlHit,istHit,btof,mtd,mtdCalib,BEmcChkStat,-evout,CorrX,OSpaceZ2,OGridLeak3D,-hitfilt"

    echo  starting new r4s PATH_DAQ=$PATH_DAQ, coreN=$coreN, execName=$EXEC_NAME, NUM_EVE=$NUM_EVE, workerName=`hostname -f`, startDate=`date`

    set daqN=$PATH_DAQ/$coreN.daq
    set coreOut=${coreN}_n$NUM_EVE

    
    #
    # - - - -  D O   N O T  T O U C H  T H I S   S E C T I O N- - - - 
    #
    
    echo os-in-shifter is
    cat /etc/*release

    echo "check if in shifter (expected 1)"
    env | grep  SHIFTER_RUNTIME
    
    whoami    
    cd ${WRK_DIR} # important

    echo  load STAR enviroment 
    set NCHOS = sl64
    set SCHOS = 64
    set DECHO = 1
    set SCRATCH = $SCRATCH/star-fakeScratch
    setenv GROUP_DIR /common/star/star${SCHOS}/group/
    source $GROUP_DIR/star_cshrc.csh    
     
    echo testing STAR setup $STAR_VER in `pwd`
    starver $STAR_VER 
    env |grep STAR

    echo 'my new STAR ver='$STAR'  test root4star '
    root4star -b -q 

    #
    # - - - -   Y O U   C A N   C H A N G E   B E L O W  - - - -
    #


    echo "fire $EXEC_NAME for coreN=$coreN "`date`
    time $EXEC_NAME -b -q bfc.C\($NUM_EVE,\"$bfcString\",\"$daqN\"\)
    echo "completed job $coreN   "`date` 

