#!/bin/bash
# Author:  Jan Balewski, 2015, 2016
# new tasks will be submitted if total # of running jobs is below the maxJobLoad limit

set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#.............
function countJobs { 
    #echo count jobs arg1=$1=
    nRun=`/usr/bin/pgrep $1 |wc -l `  
    echo  pgrep nRun=$nRun vs. limit=$maxJobLoad
}

#.............
function getRAM { 
    #echo count jobs arg1=$1=
    freeRam=`free -g |grep "cache:" |  awk  '{ print $4}'`
    echo  current freeRam_GB=$freeRam  vs. limit=$minFreeRam
}

#.............
function getLoad { 
    #echo getLoad 
    ln=`uptime | awk -F'[a-z]:' '{ print $2}'`
    #echo ln=$ln
    w=`echo $ln |  cut -f1 -d\ ` 
    #echo w=$w
    wLoad=`echo $w | cut -f1 -d.`
    echo current node w-load=$wLoad  vs. limit=$maxWLoad
}

#.............
function prNodeHealth {
    echo "**** check_$1 node health "`hostname -f`"  "`date`
    echo "free -g"
    free -g
    top bn1 |head -n 7
    top bn1 |grep $execName |grep $USER |nl
    #ps -ef |grep LUXSimExec |nl
    #/usr/bin/pgrep  $jobName |nl  
}


#=============
#   MAIN  
#=============
TASK_LIST=${1-fixeMe1}
totJobLimit=${TOT_JOB_LIMIT-999888}
maxJobLoad=${MAX_JOBS_LOAD-8}
maxWLoad=${MAX_W_LOAD-9}
execName=$EXEC_NAME

echo star-pump-1Node start maxJobLoad=$maxJobLoad,maxWLoad=$maxWLoad
echo "execName=$execName, totJobLimit=$totJobLimit, running  in "`pwd`

# do not change timing below
jobDelay=2 # (sec) launch
checkDelay=20 # (sec) monitor
minFreeRam=3  #GB
nJobs=0
k=0
myDelay=$jobDelay
echo starting submitting from  taskList=$TASK_LIST "..."
while read line ; do 
    #echo line=$line
    if [[ $line == "#"* ]]; then
	echo "skip line:" $line
	continue
    fi

    if [ $nJobs -ge $totJobLimit ] ; then
	echo "MAIN: totJobLimit=$nJobs reached, skip remining of task list"
	break
    fi

    # try to *not* submit new job - machine may be busy
    while true ; do 
	sleep $myDelay
	countJobs $execName
	getLoad
	getRAM	
	echo "  checkA_$k tot nJobs=$nJobs ,  sleep $myDelay seconds ..."`date`
	k=$[ $k +1 ]
	if [ $[ $k%10 ] -eq 5 ] ; then   prNodeHealth $k ;  fi

	if [   $nRun -ge $maxJobLoad   ] ; then
	    myDelay=$checkDelay
	    continue
	fi
       
	if [ $wLoad  -ge $maxWLoad  ] ; then
            echo too_high w-load $wLoad  ", postpone job submission"
	    myDelay=$checkDelay
            continue       
	fi

	if [ $freeRam  -le $minFreeRam  ] ; then
            echo too_low free RAM_GB=$freeRam  ", postpone job submission"
	    myDelay=$checkDelay
            continue       
	fi
	break # no reason to slack, fire now job
    done

    # prepare to fire next jobs, '&' is needed to convert from format used by taskFarmer
    job=$line' &'
    echo myjob to be fired is =$job=
    eval $job 
    nJobs=$[ $nJobs + 1 ]
    # simple math to compute larger delay if # of jobs is close to max w-load or free RAM
    myDelay=$checkDelay
    del=$[  $maxWLoad - $wLoad ]
    if [ $freeRam -lt $del ] ; then del=$freeRam  ; fi
    if [ $del -gt 0 ] ; then  myDelay=$[ $jobDelay  + 60/$del ] ; fi
    echo w=$wLoad ram=$freeRam  nr=$nRun  del=$del now sleep for myDelay=$myDelay
done <$TASK_LIST


echo "info: $nJobs jobs submitted, wait for completion"
sleep 60  # for a good measure

prNodeHealth $k

while [ true ] ; do
    countJobs $execName
    getLoad
    getRAM	

    k=$[ $k +1 ]
    if [ $[ $k%10 ] -eq 0 ] ; then   prNodeHealth $k ;  fi
    if [   $nRun -le 0   ] ; then
	break
    fi
    echo "   checkB_$k finishing jobs "`date`

    sleep $checkDelay
    continue
done
echo "pump jobs done "`date`" pwd="`pwd`
du -hs .
ls -lh *
