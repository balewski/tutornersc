###   FAQ - runing STAR jobs on Cori in Shifter under SLURM

 * Q1: job submission error:
```bash
sbatch: error: Batch job submission failed: Unspecified error
```
A1: node type/ job length/ partition - mismtach. Perhaps Cori config is executed on Edison? 

 * Q2: job submission hangs, no output from OS
 
 A2: check if machine is down on [MOTD] [http://www.nersc.gov/live-status/motd/]
 
 * Q3:  BFC job log file says: 

 ```bash
 bhla bhla   shifter: command not found
 ```

 A3: Most likely you run on Edison and the shifter module was not loaded in the slurm job, activate line 'module shifter load'
 
  * Q4: Jupyter page is not working.
   
  A4: check if Cori is donw on MOTD (see A2)
  
  * Q5: error message ibn slurm log file

  ```bash
  bhla bhla  many connection errors; unblock with mysqladmin  bhla
  ```

  A5:  You are running on Edsion which doe snot have mysql on worker nodes. Monitoring of DB connections will not work, the r4s jobs will run just fine.

 * Q6: How many mpp hours cost my job? (a runnig or completed completed job).
 A6: ssh to machine you run the job on (Cori or Edsion), execute this command

```bash
 
cori09:~>  sacct --format=job,user,submit,start,end,exitcode,nnodes,alloccpus,timelimit,cputime,state,maxvmsize,qos -j 4708393
       JobID      User              Submit               Start                 End ExitCode   NNodes  AllocCPUS  Timelimit    CPUTime      State  MaxVMSize        QOS 
------------ --------- ------------------- ------------------- ------------------- -------- -------- ---------- ---------- ---------- ---------- ---------- ---------- 
4708393           zhux 2017-04-26T01:22:27 2017-04-26T01:22:30 2017-04-26T01:51:42      0:0        2        128   00:29:00 2-14:17:36    TIMEOUT                 debug 
4708393.bat+           2017-04-26T01:22:30 2017-04-26T01:22:30 2017-04-26T01:51:55     0:15        1         64            1-07:22:40  CANCELLED    490316K            
4708393.ext+           2017-04-26T01:22:30 2017-04-26T01:22:30 2017-04-26T01:51:54      0:0        2        128            2-14:43:12  COMPLETED    215812K            
4708393.0              2017-04-26T01:23:05 2017-04-26T01:23:05 2017-04-26T01:51:57      0:0        1         64            1-06:47:28  COMPLETED  66990132K    
4708394.        
 ```

 This particular job run on 2 Haswell nodes, has not completed within requested 29 minutes and was killed w/ 'TIMEOUT'. The mpp cost of this job is 2 days 14 cpu hours. 
 The math  : 0.5h * 2 nodes * 32 cores = 32 'raw core hours'.
 The charg factor for Cori is 2.5, so this job costs 80 mpp hours.
 
 Alternatively, for a **completed** job you can visit myNersc web page:
 https://my.nersc.gov/

 Goto 'Jobs', goto 'Completed jobs' , fill user name. You may further filter jobs to find the one you care about. You will find similar information:

```bash
Completion Time	04/26/17 01:51
Nodes	2
CPUs	64
Wall Hours Used	0.487
Wall Hours Requested	0.483
Raw Core Hours Used	31.15
```

 * Q7: can I request interactive session in shared queue to save mpp hours rather then using debug queue giving me the full 32 phys core node?
 
 A2: yes. But you may wait longer for the session to start

```bash
# full node (32 phys core) session:
 salloc --nodes 1 --partition debug  --time 15 -C haswell  --image=custom:pdsf-sl64-star:v3

# frugal 1 phys core cession ( 2 vCores)
 salloc --tasks 1 --partition shared  --time 15 -C haswell  --image=custom:pdsf-sl64-star:v3
```