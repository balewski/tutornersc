#!/bin/sh
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

echo test job-pump 

# optimize  jobPump

export MAX_JOBS_LOAD=60 
export MAX_W_LOAD=60
export TOT_JOB_LIMIT=20

export EXEC_NAME=vet3.exe
export RUN_TIME=3

echo fire job pump :
././starUtilPump.sh  vetTask.list

