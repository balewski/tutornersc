#!/bin/bash

python ./prepEmbedTaskList.py \
 -trg production_pp200_2015 \
 -production P16id -lib SL16d \
 -mixer StRoot/macros/embedding/bfcMixer_Tpx.C -prodname P16idpp200 \
 -r 20163401 \
 -geantid 169 -particle Psi2SMuMu -mode FlatPt \
 -trigger 470602 -trigger 480602 -trigger 490602 \
 -z 200.0 -vrcut 100.0 \
 -ymin -0.8 -ymax 0.8 -ptmin 0 -ptmax 12.0 \
 -mult 8 \
 -local \
 -daq /global/projecta/projectdirs/starprod/daq/2015/pp200_mtd \
 -tag /global/projecta/projectdirs/starprod/tags/2015/pp200_mtd \
 -fSetRange 901-902 \
 -nEve 10 \
# -outPath /global/project/projectdirs/mpccc/balewski/out1 \
# -endGameOpt 

# note, one embedding events takes 1-2 minutes, debug queue has 30 minutes timeout.

