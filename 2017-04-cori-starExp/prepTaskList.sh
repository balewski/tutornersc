#!/bin/sh
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#-------------
function buildTaskList  { 
    daqN=$1
    mxEve=$2
    coreN=`echo $daqN | cut -f1 -d.`
    #echo $nj  coreN=$coreN, nEve=$nEve
    echo "shifter  /bin/tcsh  \${WRK_DIR}/$TASK_SCRIPT $coreN >&   \${WRK_DIR}/logs/${coreN}.r4sLog" >> $TASK_LIST

}

#================
#  MAIN
#================

inpCsvF=${1-fixMe1}
TASK_SCRIPT=${2-fixMe2}
TASK_LIST=${3-new.list}

rm -rf $TASK_LIST

mxJ=5000
nj=0
echo preparing  taskList=$TASK_LIST 
while read line ; do
    echo line=$line
    if [[ $line == "#"* ]]; then
        echo "skip line:" $line
        continue
    fi
    if [ $nj -ge $mxJ ]; then break ; fi
    
    buildTaskList $line 

    nj=$[ $nj +1 ]
done <$inpCsvF
echo generated $nj  in $TASK_LIST
head -n4 $TASK_LIST

