#!/bin/sh
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

echo execute r4sTask on PDSF interactive node, you must run this script in bash

export NUM_EVE=5
export STAR_VER=SL16d
export EXEC_NAME=root4star
export WRK_DIR=~/tmp

#./r4sTask_bfc.csh /global/projecta/projectdirs/starprod/daq/2015/pau200_bht1_adc st_physics_adc_16129049_raw_5500004 

# - - - embedding testing - - - 
./r4sTask_embed.csh st_mtd_adc_16114048_raw_2500009 101

